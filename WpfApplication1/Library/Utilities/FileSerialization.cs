﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    public class FileSerialization
    {
        public string _fileName { get; set; }
        public object _data { get; set; }
        public object _defaultData { get; set; }
        public bool isNew;

        /// <summary>
        /// Serializes an object to a file and also loads it. Cast object yourself.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="data"></param>
        /// <param name="defaultData">Default data to make if file doesn't exist</param>
        public FileSerialization(string fileName, object data, object defaultData)
        {
            _fileName = fileName;
            _data = data;
            _defaultData = defaultData;
        }

        public object load()
        {
            object data = new object();
            if (File.Exists(_fileName))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(_fileName, FileMode.Open);
                data = bf.Deserialize(file);
                file.Close();
                _data = data;
                return _data;
            }
            isNew = true;
            _data = _defaultData;
            save();
            return _data;
        }

        public void save()
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(_fileName);
            bf.Serialize(file, _data);
            file.Close();
        }
    }
}
