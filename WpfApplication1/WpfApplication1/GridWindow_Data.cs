﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Data;
using System.Windows.Controls;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Linq;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for GridWindow.xaml
    /// </summary>
    public partial class GridWindow : Window
    {
        //private class dataResult
        //{
        //    public readonly DataTable original;
        //    public readonly DataTable modified; //doesn't include entry
        //    public readonly int count;
        //    public readonly int view;

        //    public dataResult(DataTable _original, int _view)
        //    {
        //        view = _view;
        //        original = _original;
        //        modified = original.Copy();
        //        if (modified.Rows.Count > 0)
        //        {
        //            modified.Columns.Remove("entryID");
        //        }
        //        count = modified.Rows.Count;
        //    }
        //}

        private class currentView
        {
            public readonly int view;
            public bool doneBuffer;
            public currentView(int _view, bool _doneBuffer)
            {
                view = _view;
                doneBuffer = _doneBuffer;
            }
        }

        private class BindingListTrackable
        {
            public readonly BindingList<bindedTable> bufferBinder;
            public int position;
            public BindingListTrackable()
            {
                position = -1; //not position yet
                bufferBinder = new BindingList<bindedTable>();
            }
        }

        private class MasterBuffers
        {
            public readonly BindingListTrackable[] BindingListTrackable;
            public readonly streamResults[] bufferStreams;
            private int currentView;
            private DataGrid mainDisplay;
            private readonly int buffers;
            private readonly int maxEntry;
            private readonly string keyword;
            private List<int> PrevBufferTracker = new List<int>();
            private readonly List<int> bufferTracker = new List<int>();
            public readonly List<currentView> views = new List<currentView>();
            public MasterBuffers(int _buffers, int _maxEntry, DataGrid _mainDisplay, string _keyword)
            {
                keyword = _keyword;
                buffers = _buffers;
                maxEntry = _maxEntry;
                mainDisplay = _mainDisplay;
                BindingListTrackable = new BindingListTrackable[buffers];
                for (int i = 0; i < BindingListTrackable.Length; i++)
                {
                    BindingListTrackable[i] = new BindingListTrackable();
                }
                bufferStreams = new streamResults[buffers];
                for (int i = 0; i < bufferStreams.Length; i++)
                {
                    bufferStreams[i] = new streamResults();
                    bufferStreams[i].position = -1;
                }
                setOrder(0);
            }
            public void SetView(int view)
            {
                setOrder(view);
            }

            private async void setOrder(int view)
            {          
                //b5 P1 C3  
                var half = buffers / 2; //buffer is always odd  
                var center = view;
                var end = center + half + 1;
                var start = center - half;
                if (start < 0)
                {
                    start = 0;
                }     
                r.f("buffer range: Start" + start + ", center " + center + ", end " + end);
                
                //copy buffertrack to prevbuffer
                PrevBufferTracker = bufferTracker.ToList();
                //for (int i = 0; i < bufferTracker.Count; i++)
                //{
                //    PrevBufferTracker.Add(bufferTracker[i]);
                //}
                r.f("PrevBuffer:");
                for (int i = 0; i < PrevBufferTracker.Count; i++)
                {
                    r.f(PrevBufferTracker[i].ToString());
                }

                //new buffer tracks base on range
                bufferTracker.Clear(); 
                for (int i = start; i < end; i++) 
                {
                    bufferTracker.Add(i);
                    addView(i);
                }

                r.f("buffer:");
                for (int i = 0; i < bufferTracker.Count; i++)
                {
                    r.f(bufferTracker[i].ToString());
                }

                var AddBufferTracker = bufferTracker.Except(PrevBufferTracker);
                var RemoveBufferTracker = PrevBufferTracker.Except(bufferTracker);
                //for (int i = 0; i < bufferTracker.Count; i++) //all buffer that will be new
                //{
                //    if (!PrevBufferTracker.Contains(bufferTracker[i]))
                //    {
                //        AddBufferTracker.Add(bufferTracker[i]);
                //    }
                //}
                //for (int i = 0; i < PrevBufferTracker.Count; i++) //all buffer that will be remove
                //{
                //    if (!bufferTracker.Contains(PrevBufferTracker[i]))
                //    {
                //        //r.f("prev " + PrevBufferTracker[i] + " is not contain in " + bufferTracker[i]);
                //        RemoveBufferTracker.Add(PrevBufferTracker[i]);
                //    }
                //}

                r.f("Current view: " + center);
                foreach (var i in AddBufferTracker)
                {
                    r.f("add: " + i.ToString());
                }
                foreach (var i in RemoveBufferTracker)
                {
                    r.f("remove: " + i.ToString());
                }

                //removes unused position in trackables
                for (int i = 0; i < BindingListTrackable.Length; i++) 
                {
                    if (RemoveBufferTracker.Contains(BindingListTrackable[i].position))
                    {
                        r.f("Removing " + BindingListTrackable[i].position);
                        stopAStream(BindingListTrackable[i].position);
                        BindingListTrackable[i].position = -1;
                        BindingListTrackable[i].bufferBinder.Clear();    
                    }
                }

                //adds working position to trackable
                foreach (var i in AddBufferTracker)
                {
                    for (int j = 0; j < BindingListTrackable.Length; j++)
                    {
                        if (BindingListTrackable[j].position == -1)
                        {
                            BindingListTrackable[j].position = i;
                            break;
                        }
                    }
                }

                await Task.Factory.StartNew(() =>
                {
                    var j = 0;
                    for (int i = start; i < end; i++)
                    {
                        if (AddBufferTracker.Contains(i))
                        {
                            startAStream(keyword, i * maxEntry, maxEntry, i);
                            //LB and UB is define by starting and how many after
                        }
                        j++;
                    }
                });
            }

            //start is the position of entry, and position is the table position
            private void startAStream(string keyword, int start, int max, int position)
            {
                for (int i = 0; i < bufferStreams.Length; i++)
                {
                    if (bufferStreams[i].position == -1)
                    {
                        bufferStreams[i].startStream(keyword, start, max, true, position);
                        return;
                    }
                }
            }

            private void stopAStream(int position)
            {
                for (int i = 0; i < bufferStreams.Length; i++)
                {
                    if(bufferStreams[i].position == position)
                    {
                        bufferStreams[i].stopStream();
                        return;
                    }
                }
            }

            public void stopAllStream() //use when closing grid window
            {
                for (int i = 0; i < bufferStreams.Length; i++)
                {
                    bufferStreams[i].stopStream();
                }
            }

            private void addView(int newView)
            {
                bool add = true;
                foreach (var item in views)
                {
                    if(item.view == newView)
                    {
                        add = false;
                        break;
                    }
                }
                if (add)
                {
                    views.Add(new currentView(newView, false));
                }
            }

            public void markDoneBufferView(int view)
            {
                foreach (var item in views)
                {
                    if (item.view == view)
                    {
                        item.doneBuffer = true;
                        break;
                    }
                }
            }

            public void StatusView(int view, out bool doneBuffer)
            {
                doneBuffer = false;
                foreach (var item in views)
                {
                    if (item.view == view)
                    {
                        doneBuffer = item.doneBuffer;
                        break;
                    }
                }
            }

        }

        private class streamResults //responsible for updating bufferBinder
        {
            public int position;
            private dbConnection db;
            private bool active;
            public void startStream(string keyword, int LB, int UB, bool _active, int _position) //this should automatically switch streams
            {
                active = _active;
                if (active)
                {
                    //always add a new connection for streaming
                    db = new dbConnection(main.mySettings);
                    position = _position;              
                    db.QueryStreamGot += push;
                    var query = db.GetSearchQueryString(keyword, LB, UB);
                    db.queryStream(query);
                    r.f(query);
                    //db.disposeConnection();
                }
            }

            public void stopStream()
            {
                if (active)
                {
                    active = false;
                    position = -1;
                    db.QueryStreamGot -= push;
                    db.queryStreamBreaker();
                    r.f("Stream stop " + position);
                    //db.disposeConnection();
                    //handles stop streaming processes 
                }
            }

            private void push(object s, dbConnection.streamData e) //triggers when a single stream enters
            {
                if (active)
                {
                    var bt = new bindedTable(e.dateText, e.subject, e.body, e.recipient, e.entryID, e.folderName, e.fileName);
                    //r.f("Table insertion: " + e.entryID);
                    JustGotUpdated(new EventStreamData(bt, position, e.completed)); //notify main thread so bindedTable can be updated
                }      
            }
            public class EventStreamData: EventArgs
            {
                public readonly bindedTable _data;
                public readonly int _index;
                public readonly bool _completed;
                public EventStreamData(bindedTable b, int index, bool completed)
                {
                    _index = index;
                    _data = b;
                    _completed = completed;
                }
            }
            public event EventHandler<EventStreamData> JustGotUpdatedE;
            protected virtual void JustGotUpdated(EventStreamData e)
            {
                if(JustGotUpdatedE != null)
                {
                    JustGotUpdatedE(this, e);
                }
            }
        }


        private class bindedTable : INotifyPropertyChanged, getMails.ImailData
        {
            public bindedTable(
                string _date,
                string _subject,
                string _body,
                string _recipient,
                string _entryID,
                string _folderName,
                string _fileName
                )
            {
                subject = _subject;
                body = _body;
                recipient = _recipient;
                entryID = _entryID;
                folderName = _folderName;
                fileName = _fileName;
                date = Convert.ToDateTime(_date);
            }
            //order: received, subject, body, recipient, folderName, fileName, entryID
            public DateTime date { get; set; }
            public string subject { get; set; }
            public string body { get; set; }
            public string recipient { get; set; }
            public string folderName { get; set; }
            public string fileName { get; set; }
            public string entryID { get; set; }

            public event PropertyChangedEventHandler PropertyChanged;
            private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }

        private class scrollTracker
        {
            private List<int> tracker { get; set; }
            private DataGrid reference;

            public scrollTracker()
            {
                tracker = new List<int>();
                tracker.Add(0); //always shown first
            }

            public void scrollNow(int currentView, DataGrid reference)
            {
                this.reference = reference;
                if (!tracker.Contains(currentView))
                {
                    tracker.Add(currentView);
                    reference.ScrollIntoView(reference.Items[0]);
                }
                else
                {
                    reference.ScrollIntoView(reference.Items.Count);
                }
            }
        }
    }
}
