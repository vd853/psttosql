﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Windows.Media;
using System.Collections.Generic;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for FileSelector.xaml
    /// </summary>
    /// 
    public partial class FileSelector : Window
    {

        private CheckListView cv;
        private bool importBegins;
        public FileSelector(string[] paths)
        {
            InitializeComponent();
            importBegins = false;
            cv = new CheckListView(new string[0], paths, list, Add, Remove, Undo, Redo, SelectAll, nSelect, NInput, Filter);

        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            cv.AddMode();
        }

        private void Remove_Click(object sender, RoutedEventArgs e)
        {
            cv.RemoveMode(); 
        }

        private void Filter_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (cv != null)
            {
                cv.TextChange();
            }
        }

        private void Close_Click_1(object sender, RoutedEventArgs e)
        {
            MainWindow.pstSelectedPaths = cv.GetCurrentList();
            importBegins = true;
            Close();
        }

        private void Redo_Click(object sender, RoutedEventArgs e)
        {
            cv.Redoing();
        }

        private void Undo_Click(object sender, RoutedEventArgs e)
        {
            cv.Undoing();
        }

        private void Filter_GotFocus(object sender, RoutedEventArgs e)
        {
            cv.OnClickFilter();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.pstSelectedPaths = null;
            Close();
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            cv.ClearFilter();
        }


        private void SelectAll_Click(object sender, RoutedEventArgs e)
        {
            cv.SelectAll();
        }

        private void Filter_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                cv.OnFilterEnter();
            }
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if (!importBegins) { MainWindow.pstSelectedPaths = null; }
        }

        private void nSelect_Click(object sender, RoutedEventArgs e)
        {
            cv.NSelect();
        }

        private void NInput_GotFocus(object sender, RoutedEventArgs e)
        {
            cv.NButtonNFocus();
        }

        private void checkboxer_Click(object sender, RoutedEventArgs e)
        {
            cv.CheckChanged();
            r.f("check changed");
        }

        private void list_MouseEnter(object sender, MouseEventArgs e)
        {
            cv.MouseEntersList();
            r.f("mouse enters");
        }
    }
}
