﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Threading;


namespace WpfApplication1
{
    static partial class main
    {
        public static int currentOpen; //keeps track of open connection, only during debug
        public static int currentPooling; //keeps track of all the pooling access before it is cleared
        public static int limitPoolingCalls = 1000; //if currentpool reach this, pooling will clear

        public static bool cancelled; //inform all import methods to stop
        //private static dbConnection sqlConnection; //not used statically
        public static configer.dbConfig mySettings;
        public static bool connectionStatus;
        //public static stats statz;
        public static throttleState statz;
        public static float importCompletedPercent;
        public static string currentImportingFile;
        //use to determine when import should be throttling
        public class throttleState
        {
            public stats statz;
            private CheckConnections connection;
            public throttleState(string ip)
            {
                statz = new stats(2000);
                connection = new CheckConnections(ip);
            }

            public bool isThrottleConnection()
            {
                var db = new dbConnection(mySettings);
                var dbActive = db.CheckConnectionQuick();
                if (dbActive) //throttled if db is not active
                {
                    db.disposeConnection();
                    return false;
                }
                db.disposeConnection();
                return true;
            }

            public bool isThrottled()
            {
                //order: ping > SQL server active > import ratio

                if (!isThrottleConnection()) //throttled if cannot ping
                {
                    //r.v("db is active");
                    if (!statz.importThrottle) //throttled if this is true
                    {
                        //r.v("no ratio blocking");
                        return false;
                    }
                }
                return true;
            }
        }

        public class CheckConnections //not use because it is too slow
        {
            private IPAddress maintainIP;
            public CheckConnections(string ip)
            {
                if (ip == "localhost")
                {
                    ip = "127.0.0.1";
                }
                maintainIP = IPAddress.Parse(ip);
            }

            public bool Ping()
            {
                Ping pinger = new Ping();
                PingReply reply = pinger.Send(maintainIP);
                if (reply.Status == IPStatus.Success)
                {
                    return true;
                }
                return false;
            }
            public bool isNetworked() //if any nic is working
            {
                return System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable();
            }
        }
        public class stats
        {
            //public float deltaTime { private get; set; }
            private int throttle;
            private int throttleTimeOut; //maximum throttle count until condition resets for unthrottling
            private int throttleTimeOutLimit = 5;
            public int throttleDelay;
            public bool measurementBegin = false;
            public int prevTableEntries { get; set; }
            public int tableEntries { get; set; }
            public int deltaTableEntries //how much has been added since import began
            {
                get
                {
                    return tableEntries - prevTableEntries;
                }
                private set {; }
            }

            public int pushed { get; set; }

            public bool importThrottle
            {
                get
                {
                    if (deltaTableEntries < 1 && !measurementBegin)
                    {
                        return false;
                    }

                    if (!measurementBegin)
                    {
                        measurementBegin = true;
                        pushed = 0;
                    }
                    var overshoot = deltaTableEntries + throttle < pushed;
                    if (overshoot && measurementBegin)
                    {
                        r.f("deltaTableEntries: " + deltaTableEntries);
                        throttleTimeOut--;
                        r.f("throttling");
                        if (throttleTimeOut < 0) //condition is reset
                        {
                            r.f("throttle time out resetted");
                            throttleTimeOut = throttleTimeOutLimit;
                            measurementBegin = false;
                            prevTableEntries = tableEntries;
                        }
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                set {; }
            }
            public stats(int _throttle)
            {
                throttleTimeOut = 60; //in seconds
                throttleTimeOut = throttleTimeOut * 3;//since it is called 3 times
                throttle = _throttle;
                throttleDelay = 1000;
                measurementBegin = false;
                pushed = 0;
                deltaTableEntries = 0;
                importThrottle = false;
            }
            public void print()
            {
                r.f("throttle impose:   " + importThrottle);
                r.f("throttle limit:    " + throttle);
                r.f("prevTableEntries:  " + prevTableEntries);
                r.f("tableEntries:      " + tableEntries);
                r.f("deltaTableEntries: " + deltaTableEntries);
                r.f("pushed:            " + pushed);
            }
        }
    }
}
