﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Windows;
using System.Windows.Media;

namespace WpfApplication1
{
    static partial class main
    {
        public static void init()
        {   
            fileLog.outlog("START", false);
            OutlookManager.openPst = new List<string>();
            cancelled = false;
            if (!OutlookManager.checkForOutlook())
            {
                var msg = MessageBox.Show("You must open Outlook. Do you want to open Outlook?", "Outlook is not open", MessageBoxButton.YesNo);
                switch (msg)
                {
                    case MessageBoxResult.None:
                        break;
                    case MessageBoxResult.Yes:
                        OutlookManager.OpenOutlook();
                        break;
                    case MessageBoxResult.No:
                        break;
                }
                if (!OutlookManager.checkForOutlook())
                {
                    Environment.Exit(0);
                    return;
                }     
            }

            //settings dependencies
            mySettings = configer.load();
            var sqlConnection = new dbConnection(mySettings);  
            fileLog.expireLog(mySettings.maxLog);
            //mySettings.tables = sqlConnection.getAllTables();
            if (!sqlConnection.checkConnection())
            {
                connectionStatus = false;
                var optionWin = new OptionTab();
                optionWin.ShowDialog();
            }
            else
            {
                connectionStatus = true;
                sqlConnection.checkIfMainExistOrCreate();
                sqlConnection.updateAllTables();
            };
            //sqlConnection.createIndex();
            sqlConnection.disposeConnection();
            var to = new throttleState(main.mySettings.server);
            to.isThrottled();
        }
    }


    public static class Mouse
    {
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool GetCursorPos(ref Win32Point pt);

        [StructLayout(LayoutKind.Sequential)]
        internal struct Win32Point
        {
            public Int32 X;
            public Int32 Y;
        };
        public static Point GetMousePosition()
        {
            Win32Point w32Mouse = new Win32Point();
            GetCursorPos(ref w32Mouse);
            return new Point(w32Mouse.X, w32Mouse.Y);
        }
    }
}
