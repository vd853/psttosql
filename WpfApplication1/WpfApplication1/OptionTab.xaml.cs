﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for OptionTab.xaml
    /// </summary>
    public partial class OptionTab : Window
    {
        public OptionTab()
        {
            InitializeComponent();
            
        }

        void anyClose()
        {
            if (main.connectionStatus)
            {
                saveSettings_email();
                select();
            }   
            saveSettings();
            if(main.connectionStatus)Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var testConnection = new main.throttleState(main.mySettings.server);
            main.connectionStatus = !testConnection.isThrottleConnection();

            tableName.IsEnabled = false;
            textBox.Text = "[Table name to be added or created]";
            getSettings();
            getSettings_email();
            if (main.connectionStatus)
            {

                refreshList();
                InitSearcher();
            }            
        }

        
    }
}
