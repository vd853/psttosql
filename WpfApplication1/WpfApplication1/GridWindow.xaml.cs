﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Data;
using System;
using System.Windows.Controls;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for GridWindow.xaml
    /// </summary>
    public partial class GridWindow : Window
    {

        //private bool bufferLock = false;
        //private bool started = false;
        //private int maxView = int.MaxValue;
        private readonly string keyword;
        ////readonly DataTable original;
        //private int rowCount;
        private int maxBuffer = 7; //must be odd and > 1, bufferTracker range
        //private dataResult[] results;
        //private List<int> bufferTracker = new List<int>();
        //private List<int> PrevBufferTracker = new List<int>(); //range of results to keep alive
        private int bufferRange = 3; //forward and backward limit to generate or delete
        private int resultLimit =200; //amount to be display on view
        //private bool maxViewUndefine = true;
        private int viewNow;
        private int viewLast;
        private int viewLastAmount;
        private bool viewLastMarked = false;
        //BindingList<bindedTable> updateTabler;
        MasterBuffers m;
        streamResults stream1;
        private scrollTracker scroller;
        public GridWindow(string _keyword)
        {
            InitializeComponent();
            bufferRange = main.mySettings.tableBuffer;
            resultLimit = main.mySettings.viewLimit;
            keyword = _keyword;
            getTitle();           

            m = new MasterBuffers(maxBuffer, resultLimit, dataGrid, _keyword);
            scroller = new scrollTracker();

            foreach (var i in m.bufferStreams)
            {
                i.JustGotUpdatedE += updateTable;
            }

            setDisplay(0);
        }

        string result(int v, int actual = -1) //returns proper result message base on view which is 0....
        {
            if (actual == -1)
            {
                actual = resultLimit;
            }
            if (viewLastMarked) //sets the actual to the last remaining results found
            {
                if (v == viewLast)
                {
                    actual = viewLastAmount;
                }
            }
            return "Result " + v * resultLimit + " - " + (v * resultLimit + actual) + " ";
        }
        void updateTable(object o, streamResults.EventStreamData e)
        {
            //r.f("table updated");
            Dispatcher.Invoke(() =>
            {
                if (e._completed) //if completed, find the corresponding binder and mark as done or last view
                {
                    for (int i = 0; i < m.BindingListTrackable.Length; i++)
                    {
                        if (e._index == m.BindingListTrackable[i].position)
                        {
                            if (m.BindingListTrackable[i].bufferBinder.Count < resultLimit)
                            {
                                if (!viewLastMarked)
                                {
                                    viewLastMarked = true;
                                    viewLast = e._index;
                                    viewLastAmount = m.BindingListTrackable[i].bufferBinder.Count;
                                    r.f("LAST VIEW: " + e._index);
                                }
                            }
                            m.markDoneBufferView(e._index);
                            r.f("Table has completed " + e._index);
                            if (e._index == viewNow) //removes the is buffering... text if it is currently shown
                            {
                                setDisplay(viewNow, true);
                            }
                            if (m.BindingListTrackable[i].bufferBinder.Count > 0)
                            {
                                buffering.Content = result(e._index, m.BindingListTrackable[i].bufferBinder.Count) +
                                                    "is ready.";
                            }
                            else
                            {
                                if (viewNow == 0 && viewNow == e._index )
                                {
                                    buffering.Content = "This search has no results";
                                }
                            }
                            break;
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < m.BindingListTrackable.Length; i++) //check all list for corresponding position to add to entry
                    {
                        if (e._index == m.BindingListTrackable[i].position)
                        {
                            r.f("Updating table " + e._index);
                            m.BindingListTrackable[i].bufferBinder.Add(e._data);
                            //r.f("Binder " + e._index + " length " + m.BindingListTrackable[i].bufferBinder.Count);
                            break;
                        }
                    }
                }
            });
        }
        private async void getTitle()
        {
            Title = "Getting result count...";
            var andor = "";
            if (main.mySettings.mustExist)
            {
                andor = " Using AND search.";
            }
            else
            {
                andor = " Using OR search.";
            }
            string titleKeyword = "";
            var t = await Task.Run(() =>
            {
                var con = new dbConnection(main.mySettings);
                var count = 0;
                try
                {
                     count = con.getUnionDataTableCount(keyword, out titleKeyword);
                }
                catch
                {
                    r.v("Your search is invalid.");
                }
                return count;
            });
            Title = "Search for " + titleKeyword + " has found +" + t + " results. From " + main.mySettings.getSearchTable().Count + " profile." + andor;
        }

        private void prevButton_Click(object sender, RoutedEventArgs e)
        {
            setDisplay(getView(false));
        }

        int getView(bool forward)
        {
            if (forward) //moving forward
            {
                if (!viewLastMarked) //no last view, keep going forward
                {
                    bool doneBuffer;
                    m.StatusView(viewNow+1, out doneBuffer);
                    if (!doneBuffer) //if next view after is buffering, don't go there
                    {
                        buffering.Content = "Next result is still buffering...";
                        return viewNow;
                    }
                    else
                    {
                        return ++viewNow;
                    }     
                }
                else //has last view, go back to zero
                {
                    if (viewNow == viewLast) //if at last view, go back to 0
                    {
                        viewNow = 0;
                        return 0;
                    }
                    else //if not at last view keep moving forward
                    {
                        return ++viewNow;
                    }
                }
            }
            else //moving backward
            {
                if (!viewLastMarked) //no last view
                {
                    if (viewNow != 0) //if not at zero, keep going backward
                    {
                        return --viewNow;
                    }
                    else //if at zero, stay there
                    {
                        return 0;
                    }
                }
                else //has last view
                {
                    if (viewNow == 0) //go to last view if at zero
                    {
                        viewNow = viewLast;
                        return viewLast;
                    }
                    else //if not at zero, keep going backward
                    {
                        return --viewNow;
                    }
                }
            }
        }
        void setDisplay(int view, bool setLabelOnly = false)
        {
            bool doneBuffer;
            m.StatusView(view, out doneBuffer);
            if (!doneBuffer)
            {
                label.Content = result(view) + "is buffering...";
            }

            if (viewLastMarked)
            {
                r.f("Last view marked");
                if (view == viewLast)
                {

                    label.Content = result(view) + "is last result.";
                }
                else
                {
                    label.Content = result(view);
                }
            }
            else
            {
                label.Content = result(view);
            }

            if (setLabelOnly) return; //use to update the is buffering... word from tableupdate

            m.SetView(view);
            for (int i = 0; i < m.BindingListTrackable.Length; i++)
            {
                if (m.BindingListTrackable[i].position == view)
                {
                    dataGrid.ItemsSource = m.BindingListTrackable[i].bufferBinder;
                    break;
                }
            }
            if(doneBuffer) scroller.scrollNow(view,dataGrid);
        }


        private void nextButton_Click(object sender, RoutedEventArgs e)
        {
            setDisplay(getView(true));
            //nextResultView();
        }

      

        private void dataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            string ID, path;
            getOpenData(out ID, out path);
            var gm = new getMails(path, getMails.getMode.openMail, ID);
        }

        void getOpenData(out string entryID, out string filePath)
        {
            int rowIndex = dataGrid.SelectedIndex;

            var b = dataGrid.ItemsSource as BindingList<bindedTable>;

            filePath = b[rowIndex].fileName;
            entryID = b[rowIndex].entryID;
        }

        /// <summary>
        /// gets value of subject or body of current selection
        /// </summary>
        /// <returns></returns>
        string quickSubjectBody()
        {
            var returnString = "";
            int rowIndex = dataGrid.SelectedIndex;
            if (rowIndex < 0) return returnString;
            int colIndex = dataGrid.CurrentCell.Column.DisplayIndex;         
            if (colIndex == 1 || colIndex == 2)
            { 
                var b = dataGrid.ItemsSource as BindingList<bindedTable>;
                if (colIndex == 1) //subject
                {
                    returnString = b[rowIndex].subject;
                }
                else
                {
                    returnString = b[rowIndex].body;
                }
            } 
            return returnString;
        }
        private void dataGrid_SizeChanged(object sender, SizeChangedEventArgs e)
        {

            r.f("GridWindow w: " + this.Width);
            r.f("GridWindow h: " + this.Height);
            return;
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            Task.Factory.StartNew(() => { m.stopAllStream(); });
            Close();
        }

        private void dataGrid_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            var s = quickSubjectBody();
            if (s != "")
            {
                var quickView = new console("Preview", ConsoleMode.quick);
                quickView.log(s);
                quickView.setLocation((int)Mouse.GetMousePosition().X, (int)Mouse.GetMousePosition().Y);
                quickView.Show();
            }
        }
    }
}
