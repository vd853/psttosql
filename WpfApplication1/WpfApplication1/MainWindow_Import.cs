﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Threading;
using System.ComponentModel;
using WpfApplication1.Utilities;
using System.IO;
using System.Linq;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private void Import_Click(object sender, RoutedEventArgs e)
        {
            if (pstFound)
            {
                var msg = MessageBox.Show("Import all PST?", "Import", MessageBoxButton.YesNoCancel);
                switch (msg)
                {
                    case MessageBoxResult.None:
                        break;
                    case MessageBoxResult.OK:
                        break;
                    case MessageBoxResult.Cancel:
                        return;
                        break;
                    case MessageBoxResult.Yes:
                        import(true);
                        break;
                    case MessageBoxResult.No:
                        var fs = new FileSelector(pstPaths);
                        fs.ShowDialog();
                        if (pstSelectedPaths == null || pstSelectedPaths.Length == 0)
                        {
                            return; //no importing
                        }else
                        {
                            import(false); //import
                        }    
                        break;
                }

                ranOnce = true;
            }
            else
            {
                log("No PST found for importing.");
            }
        }

        List<getPST.PSTProfileModel> OrganizeParticalImport()
        {
            revertablePSTProfile = new List<getPST.PSTProfileModel>(getPst.PSTProfile);
            getPst.RemoveFrom(pstSelectedPaths);
            var executeProfiles = getPst.GetExecuteList();
            return executeProfiles;
        }
        void import(bool importAll)
        {
            List<getPST.PSTProfileModel> ToBeImported;
            main.cancelled = false;
            completedPST = new List<string>();
            sw.Start();
            lockWhileImporting(true);
            if (importAll)
            {
                log("===== Importing all PST =====");
                ToBeImported = getPst.PSTProfile;
            }else
            {
                ToBeImported = OrganizeParticalImport();
                foreach (var item in pstSelectedPaths)
                {
                    log("===== Import " + item + " =====");
                }
            }
            worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = true;
            worker.DoWork += importWorker;
            worker.RunWorkerCompleted += importWorker_Completed;
            worker.RunWorkerAsync(ToBeImported);

            worker2 = new BackgroundWorker();
            worker2.WorkerSupportsCancellation = true;
            worker2.DoWork += checkEntries;
            worker2.RunWorkerAsync(DoWorkEventArgs.Empty);

            main.statz = new main.throttleState(main.mySettings.server);

            entryUpdaterS = new CancellationTokenSource();
            entryUpdaterCT = entryUpdaterS.Token;
            entryUpdater = Task.Factory.StartNew(() => {
                var d = new dbConnection(main.mySettings); //is only open once
                //statz.statz. is use to determine import/export speed while statz is for general throttling
                main.statz.statz.prevTableEntries = main.statz.statz.tableEntries = d.getTableEntryCount();
                while (true)
                {  
                    main.statz.statz.tableEntries = d.getTableEntryCount();
                    Thread.Sleep(2000);
                }
            }, entryUpdaterCT);
        }
        void checkEntries(object s, DoWorkEventArgs e)
        {
            while (true)
            {
                if (worker2.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }
                Dispatcher.Invoke(() =>
                {
                    //ilog("--------------------");
                    //ilog("Imports: " + main.statz.pushed.ToString());
                    //ilog("Entries: " + main.statz.tableEntries.ToString());
                    L1_import.Content = main.statz.statz.pushed.ToString();
                    L2_entry.Content = main.statz.statz.tableEntries.ToString();
                    if (main.statz.isThrottled())
                    {
                        //ilog("Import Throttled");
                        L3_throttled.Content = "YES";
                        L3_throttled.Foreground = Brushes.Red;

                    }
                    else
                    {
                        L3_throttled.Content = "NO";
                        L3_throttled.Foreground = Brushes.Black;
                    }
                    if (main.statz.statz.measurementBegin)
                    {
                        L1.Content = "Import";
                    }
                    else
                    {
                        L1.Content = "Check";
                    }
                });
                Thread.Sleep(200);
            }
        }

        void importWorker_OpenClosePST(List<getPST.PSTProfileModel> paths)
        {
            try
            {
                OutlookManager.openPst.Clear();
                foreach (getPST.PSTProfileModel p in paths)
                {
                    OutlookManager.addStore(p.fileLocation);
                }
                var closer = new OutlookManagerI();
                closer.CloseEveryStore();
                OutlookManager.openPst.Clear();
                Dispatcher.Invoke(() => { ilog("Reclosing PSTs"); });
            }
            catch
            {
                r.v("Some PST are missing, remove them manually from Outlook before importing. All import will be cancelled.");
                cancelImport();
                return;
            }
        }

        void importWorker_verifyEntire(List<getPST.PSTProfileModel> paths)
        {
            var limitToRestart = 10;
            var count = 0;
            var previousFailed = "";
            Dispatcher.Invoke(() => { ilog("Veryifying PST"); });
            for (var i = 0; i < paths.Count; i++)
            {
                var gm = new getMails(paths[i].fileLocation, getMails.getMode.verify);
                if (!gm.storeIsOpenAble)
                {               
                    fileLog.outlog(paths[i].fileLocation + " is NOT openable", true);
                    if (previousFailed != paths[i].fileLocation) //one chance to succeed or this will be pass
                    {
                        previousFailed = paths[i].fileLocation;
                        Dispatcher.Invoke(() =>
                        {
                            ilog("Cannot open: " + paths[i].fileLocation + ", attempting to restart Outlook.");
                        });               
                        Dispatcher.Invoke(() =>
                        {
                            ilog("Please wait for Outlook to restart. This will take a minute.");
                        });
                        i--;
                        count = 0;
                        OutlookManager.RestartOutlook();
                    }
                    else
                    {
                        Dispatcher.Invoke(() =>
                        {
                            ilog("PST has failed to open twice and will be skipped.");
                        });
                        paths[i].verified = false;
                    }  
                }
                else
                {
                    fileLog.outlog(paths[i].fileLocation + " is openable", false);
                    paths[i].verified = true;
                }




                if (!r.CheckFileCorrupted(paths[i].fileLocation))
                {
                    fileLog.outlog("Something wrong with " + paths[i].fileLocation, true);
                }
                else
                {
                    r.f(i + ": validation pass " + paths[i].fileLocation);
                }

                count++;
                if (count == limitToRestart)
                {
                    count = 0;
                    Dispatcher.Invoke(() =>
                    {
                        ilog("Outlook will restart to refresh.");
                        fileLog.outlog("Outlook will restart to refresh.", false);
                    });
                    OutlookManager.RestartOutlook();
                }
            }
        }

        bool importWorker_VerifySingle(string pstPath, ref int currentInterval)
        {
            //read-only test
            if (!r.CheckFileCorrupted(pstPath))
            {
                fileLog.outlog("Skipping, cannot read " + pstPath, true);
                return false;
            }

            r.f("Read validation pass" + pstPath);

            //open pst test
            
            var attempts = 1;
            for (int i = 0; i < attempts+1; i++)
            {
                var gm = new getMails(pstPath, getMails.getMode.verify);
                if (gm.storeIsOpenAble) break;

                //condition if not openable
                currentInterval = 0; //resets the refresh outlook interval since outlook will restart here
                fileLog.outlog(pstPath + " is NOT openable", true);
                if (i != attempts)
                {
                    Dispatcher.Invoke(() =>
                    {
                        var msg = "Cannot open: " + pstPath + ", attempting to restart Outlook.";
                        ilog(msg);
                        fileLog.outlog(msg, true);
                    });
                    Dispatcher.Invoke(() =>
                    {
                        ilog("Please wait for Outlook to restart. This will take a minute.");
                    }); 
                    OutlookManager.RestartOutlook();
                }
                else
                {
                    Dispatcher.Invoke(() =>
                    {
                        ilog("PST has failed to open twice and will be skipped.");
                    });
                    fileLog.outlog(pstPath + " has failed to open " + attempts + " times and will be skipped.",true);
                    return false;
                }
            }
            return true;
        }

        void importWorker(object s, DoWorkEventArgs e)
        {
            var refreshInterval = 15;
            var currentInterval = 0;
            var defaultTable = main.mySettings.defaultProfile;
            List<getPST.PSTProfileModel> paths = e.Argument as List<getPST.PSTProfileModel>;

            importWorker_OpenClosePST(paths);

            main.importCompletedPercent = 0;
            var complete = 0;
            foreach (getPST.PSTProfileModel p in paths)
            {
                r.f("There are N paths: " + paths.Count);

                //creates table
                if(main.mySettings.importSplit) configer.SwitchTable(p.profileName);
                
                //track pst name, and log complete percentage
                main.currentImportingFile = System.IO.Path.GetFileName(p.fileLocation);
                Dispatcher.Invoke(() => { ilog("Completed: " + main.importCompletedPercent.ToString("F0") + "%" + " | Importing: " + main.currentImportingFile); });

                //verification
                var PSTisGood = importWorker_VerifySingle(p.fileLocation, ref currentInterval);

                //actual importing execution
                if (PSTisGood)
                {
                    //restart UI counter with new table
                    var d = new dbConnection(main.mySettings);
                    main.statz.statz.prevTableEntries = main.statz.statz.tableEntries = d.getTableEntryCount();
                    d.disposeConnection();

                    //execute import for this PST
                    r.f(r.TruncateString(p.fileLocation, 10) + " PST will execute");
                    var gm = new getMails(p.fileLocation, getMails.getMode.getFolderSingle); //comment this line and next to run a quick test
                    if (!gm.actionCancelled) gm.closeStore();//if true gm could not open store so does nothing
                }
                else
                {
                    //PST will be marked and skipped
                    p.verified = false;
                }

                //cancellation
                if (worker.CancellationPending)
                {
                    configer.SwitchTable(defaultTable);
                    e.Cancel = true;
                    return;
                }

                r.f(p.fileLocation + " is completed");
                Dispatcher.Invoke(() => {
                    fileLog.outlog(p.fileLocation + " export is complete.", false);
                });
                    

                if (main.statz.isThrottled() && !worker.CancellationPending) //slow down
                {
                    r.f("Importer throttle imposed");
                    ////full stop if disconnected///
                    var reportConnection = false; //use to report disconnection only once on the filelog
                    while (main.statz.isThrottleConnection()) //complete stop
                    {
                        if (!reportConnection)
                        {
                            reportConnection = true;
                            fileLog.outlog("Unable to connect to database.", true);
                        }
                        r.f("No connection to db");
                        Thread.Sleep(3000);
                    }
                    if (reportConnection)
                    {
                        fileLog.outlog("Database has been succesfully reconnected.", false);
                    }
                    Thread.Sleep(main.statz.statz.throttleDelay);
                }  
                complete++;
                if (complete > 0)
                {
                    main.importCompletedPercent = 100 * (float)complete / paths.Count;
                }
                completedPST.Add(p.fileLocation);
                //referesh check, close outlook and empty db pool
                currentInterval++;
                if (currentInterval == refreshInterval)
                {
                    Dispatcher.Invoke(() => { ilog("Outlook will restart to refresh."); });

                    //clears connection pool during the middle of outlook rest time
                    Task.Run(() =>
                    {
                        Task.Delay(40000);//only because outlook will be closed half this time
                        dbConnection.ResetPool();
                    });

                    OutlookManager.RestartOutlook(true);
                    Thread.Sleep(2000);
                    currentInterval = 0;
                }
                var con = new dbConnection(main.mySettings);
                con.CheckDeleteEmptyTable(p.profileName);
                con.disposeConnection();
            } 
            configer.SwitchTable(defaultTable);
        }
        void importWorker_Completed(object s, RunWorkerCompletedEventArgs e)
        {
            Dispatcher.Invoke(() => {
                cancelOnScreenLogger();
                if (!main.cancelled)
                {
                    var msg = "===== Importing has completed, Elapsed Time (m): " + sw.Elapsed.TotalMinutes.ToString("F2") + " =====";
                    Task.Run(() =>
                    {
                        var con = new dbConnection(main.mySettings);
                        con.optimizeTable();
                        con.disposeConnection();
                    });
                    sw.Stop();
                    log("Optimizing...");
                    fileLog.outlog(msg, false);
                    log(msg);
                    mail.send(msg);
                }
                else
                {
                    var msg = "===== Importing has been canceled, Elapsed Time (m): " + sw.Elapsed.TotalMinutes.ToString("F2") + " =====";
                    sw.Stop();
                    fileLog.outlog(msg, false);
                    log(msg);
                    mail.send(msg);
                }
                if (fileLog.errorCount > 0)
                {
                    log(fileLog.errorCount.ToString() + " error were logged, check Log folder: " + fileLog.logFileName + ".txt");
                }
                sw.Reset();
                lockWhileImporting(false);
                getPst.PSTProfile = revertablePSTProfile; //reverts back to full discovery list
                pstPaths = getPst.getAllPST();
                if (!main.cancelled) //determine if import was 100% compelte
                {
                    if (main.importCompletedPercent < 99)
                    {
                        reinitiateImport();
                    }
                }
            });
        }

        void reinitiateImport()
        {
            log("Import will retry because it was incomplete!");
            fileLog.outlog("Import has failed to reach 100%, attempting to retry import using last import PST. This maybe cause by Outlook hanging", true);
            OutlookManager.RestartOutlook();
            var unimportedList = pstPaths.Except(completedPST); //get all pstpaths execpt completed
            pstSelectedPaths = unimportedList.ToArray();
            import(false);
        }

        private async void cancel_Click(object sender, RoutedEventArgs e)
        {
            log("Importing canceling...");
            await Task.Factory.StartNew(()=>cancelImport());
        }

        void cancelImport()
        { 
            main.cancelled = true;
            if (worker2 != null)
            {
                if (worker2.IsBusy)
                {
                    worker2.CancelAsync();
                }
            }
            if (worker != null)
            {
                if (worker.IsBusy)
                {
                    worker.CancelAsync();
                }
            }
            var db = new dbConnection(main.mySettings);
            //db.optimizeTable();
            db.disposeConnection();
        }

        void cancelOnScreenLogger()
        {
            entryUpdaterS.Cancel();
            if (worker2 != null)
            {
                if (worker2.IsBusy)
                {
                    worker2.CancelAsync();
                }
            }
        }

        
    }
}
