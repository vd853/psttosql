﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Windows.Media;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utilities;

namespace WpfApplication1
{

    public partial class OptionTab : Window
    {
        private CheckListView cv;
        void readSettings()
        {
            viewLimit.Text = main.mySettings.viewLimit.ToString();
            tableBuffer.Text = main.mySettings.tableBuffer.ToString();
        }

        void writeSettings()
        {
            main.mySettings.viewLimit = StringFormatter.stringToInt(viewLimit.Text);
            main.mySettings.tableBuffer = StringFormatter.stringToInt(tableBuffer.Text);
        }
        void InitSearcher()
        {
            cv = new CheckListView(main.mySettings.getSearchTable().ToArray(), main.mySettings.getNoSearchTable().ToArray(), list, Add, Remove, Undo, Redo, SelectAll, NSelect, NInput,Filter);
            readSettings();
        }
        private void NSelect_Click(object sender, RoutedEventArgs e)
        {
            cv.NSelect();
        }
        private void NInput_GotFocus(object sender, RoutedEventArgs e)
        {
            cv.NButtonNFocus();
        }
        private void Add_Click(object sender, RoutedEventArgs e)
        {
            cv.AddMode();
        }

        private void Remove_Click(object sender, RoutedEventArgs e)
        {
            cv.RemoveMode();
        }

        private void Undo_Click(object sender, RoutedEventArgs e)
        {
            cv.Undoing();
        }

        private void Redo_Click(object sender, RoutedEventArgs e)
        {
            cv.Redoing();
        }

        //private void Close_Click(object sender, RoutedEventArgs e)
        //{
        //    var list = new List<string>(cv.GetCurrentList());
        //    if (list.Count > 15)
        //    {
        //        r.v("You cannot select more than 15");
        //    }
        //    else
        //    {
        //        main.mySettings.setSearchTables(list);
        //        anyClose();
        //    } 
        //}
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
        private void NInput_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!StringValidator.OnlyPostiveNegativeNumbers(NInput.Text))
            {
                NInput.Text = "";
                r.v("Only negative or positive numbers are allow.");
            }
        }
        private void SelectDelete_Click(object sender, RoutedEventArgs e)
        {
            var list = new List<string>(cv.GetCurrentList());
            if (list.Contains(dbConnection.mainTable))
            {
                r.v("You cannot delete: " + dbConnection.mainTable);
                return;
            }
            var jList = String.Join(Environment.NewLine, list);
            var msg = MessageBox.Show("You are about to delete: " + Environment.NewLine + Environment.NewLine + jList, "Import", MessageBoxButton.YesNo);
            switch (msg)
            {
                case MessageBoxResult.Yes:
                    var con = new dbConnection(main.mySettings);
                    list.ForEach(x =>
                    {
                        deleting(x);
                    });
                    con.disposeConnection();
                    r.v("Option will close to refresh changes.");
                    anyClose();
                    break;
                case MessageBoxResult.No:
                    break;
                default:
                    break;
            }
        }

        private void ClearChecks_Click(object sender, RoutedEventArgs e)
        {
            cv.ClearFilter();
        }

        private void Filter_GotFocus(object sender, RoutedEventArgs e)
        {
            cv.OnClickFilter();
        }

        private void Filter_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (cv != null)
            {
                cv.TextChange();
            }
        }
        private void selectAll_Click(object sender, RoutedEventArgs e)
        {
            cv.SelectAll();
        }
        private void Filter_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                cv.OnFilterEnter();
            }
        }

        private void viewLimit_LostFocus(object sender, RoutedEventArgs e)
        {
            writeSettings();
            readSettings();
        }

        private void tableBuffer_LostFocus(object sender, RoutedEventArgs e)
        {
            writeSettings();
            readSettings();
        }

        private void CloseSearch_Click(object sender, RoutedEventArgs e)
        {
            var list = new List<string>(cv.GetCurrentList());
            if (list.Count > 15)
            {
                r.v("You cannot select more than 15");
            }
            else
            {
                main.mySettings.setSearchTables(list);
                anyClose();
            }
        }
        private void list_MouseEnter(object sender, MouseEventArgs e)
        {
            cv.MouseEntersList();
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            cv.CheckChanged();
        }
    }
}
