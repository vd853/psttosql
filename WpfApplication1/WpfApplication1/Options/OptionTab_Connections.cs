﻿using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Utilities;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for OptionTab.xaml
    /// </summary>
    public partial class OptionTab : Window
    {
        void getSettings()
        {

            SettingsToField();
            if (main.connectionStatus)
            {
                updateEntry();
                alert.Content = "";
                r.f("Option for connection is okay.");
            }
            else
            {
                UIDisconnected(true);
            }
            
        }

        void UIDisconnected(bool isDisconnected)
        {
            Clear.IsEnabled = !isDisconnected;
            deleteAll.IsEnabled = !isDisconnected;
            Opt.IsEnabled = !isDisconnected; //optimize button
            if (isDisconnected)
            {
                alert.Visibility = r.SetVisibility(true);
                alert.Content = "Connection has failed. Update your settings.";
            }
            else
            {
                alert.Visibility = r.SetVisibility(false);
            }
            
        }
        void updateEntry()
        {
            var db = new dbConnection(main.mySettings);
            Entry.Content = db.getTableEntryCount().ToString();
            db.disposeConnection();
        }
        configer.dbConfig FieldToSettings()
        {
            configer.dbConfig settings = new configer.dbConfig();
            settings = main.mySettings;
            settings.db = dbName.Text;
            settings.importSplit = checkBox_sp.IsChecked.Value;
            settings.maxLog = StringFormatter.stringToInt(maxLogTextbox.Text);
            settings.server = Server.Text;
            settings.defaultProfile = tableName.Text;
            settings.user = user.Text;
            settings.password = passwordBox.Password;
            settings.searchPath = MainWindow.w.path.Text;
            settings.multithread = MainWindow.w.mtCheckBox.IsChecked.Value;
            settings.trayMinimize = checkBox_tm.IsChecked.Value;
            return settings;
        }
        configer.dbConfig SettingsToField()
        {
            configer.dbConfig settings = new configer.dbConfig();  
            settings = main.mySettings;
            checkBox_tm.IsChecked = settings.trayMinimize;
            dbName.Text = settings.db;
            checkBox_sp.IsChecked = settings.importSplit;
            maxLogTextbox.Text = settings.maxLog.ToString();
            Server.Text = settings.server;
            tableName.Text = settings.defaultProfile;
            user.Text = settings.user;
            passwordBox.Password = settings.password;
            MainWindow.w.path.Text = settings.searchPath;
            MainWindow.w.mtCheckBox.IsChecked = settings.multithread;
            return settings;
        }
        void saveSettings(bool withDialog = false)
        {     
            var settings = FieldToSettings();
            var db = new dbConnection(settings);
            if (db.checkConnection())
            {
                main.mySettings = settings;
                //db.updateAllTables();
                configer.save(main.mySettings);
                updateEntry();
                refreshList();
                InitSearcher();
                if (!main.mySettings.importSplit)
                {
                    r.e("All imports will go into profile: " + main.mySettings.defaultProfile);
                }
                if (withDialog)
                {
                    r.v("Connection was successful.");
                }
                UIDisconnected(false);
            }
            else
            {
                UIDisconnected(true);
                TabController.SelectedIndex = 0; //selects connection tab
                System.Windows.MessageBox.Show("Settings has failed to connect, reverting settings.");
                getSettings();
            }
            db.disposeConnection();
               
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            anyClose();   
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            var db = new dbConnection(main.mySettings);
            db.recreateTable();
            Thread.Sleep(500);
            updateEntry();
            Clear.IsEnabled = false;
            Opt.IsEnabled = false;
            db.disposeConnection();
        }

        private void Opt_Click(object sender, RoutedEventArgs e)
        {
            Task.Run(() =>
            {
                r.v("Optimization in progress..");
                var db = new dbConnection(main.mySettings);
                db.optimizeTable();
                db.disposeConnection();
                r.v("Optimization is complete.");
            });          
            Opt.IsEnabled = false;
        }

        private void deleteAll_Click(object sender, RoutedEventArgs e)
        {
            var response = MessageBox.Show("Delete all profiles permanently?", "Delete", MessageBoxButton.YesNo);
            switch (response)
            {
                case MessageBoxResult.None:
                    break;
                case MessageBoxResult.OK:
                    break;
                case MessageBoxResult.Cancel:
                    break;
                case MessageBoxResult.Yes:
                    var con = new dbConnection(main.mySettings);
                    con.deleteAllTable();
                    con.disposeConnection();
                    r.v("Option will close to refresh changes.");
                    anyClose();  
                    break;
                case MessageBoxResult.No:
                    break;
                default:
                    break;
            }
        }
        private void Test_Connection_Click(object sender, RoutedEventArgs e)
        {
            saveSettings(true);
            if (main.connectionStatus)
            {
                refreshList();
            }
        }

        private void checkBox_tm_Checked(object sender, RoutedEventArgs e)
        {
            if (checkBox_tm.IsChecked.Value)
            {
                MinimizeToTray.Enable(MainWindow.w);        
            }
        }
        private void checkBox_tm_Unchecked(object sender, RoutedEventArgs e)
        {
            r.v("You must restart this application to disable tray minimize.");
        }

        private void dbNameLabel_Click(object sender, RoutedEventArgs e)
        {
            var response = MessageBox.Show("YES to create database or NO to delete database", "Database", MessageBoxButton.YesNoCancel);
            switch (response)
            {
                case MessageBoxResult.None:
                    break;
                case MessageBoxResult.OK:
                    break;
                case MessageBoxResult.Cancel:
                    break;
                case MessageBoxResult.Yes:
                    if (dbName.Text == dbConnection.maindb)
                    {
                        r.v("Cannot create database " + dbConnection.maindb + ". Use another name.");
                        return;
                    }
                    var con1 = new dbConnection(main.mySettings);
                    if (con1.createDatabase(dbName.Text))
                    {
                        anyClose();
                    }
                    else
                    {
                        r.v("Database already exist or name is invalid.");
                    }
                    break;
                case MessageBoxResult.No:
                    if (dbName.Text == dbConnection.maindb)
                    {
                        r.v(dbConnection.maindb + " can never be deleted.");
                        return;
                    }
                    var con2 = new dbConnection(main.mySettings);
                    if (con2.dropDatabase(dbName.Text))
                    {
                        dbName.Text = dbConnection.maindb;
                        anyClose();
                    }
                    else
                    {
                        dbName.Text = dbConnection.maindb;
                        r.v("Database does not exist.");
                        return;
                    }
                    break;
            }
        }
    }
}
