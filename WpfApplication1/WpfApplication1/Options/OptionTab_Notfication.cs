﻿using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Utilities;
using WpfApplication1.Utilities;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for OptionTab.xaml
    /// </summary>
    public partial class OptionTab : Window
    {
        void getSettings_email()
        {
            SettingsToField_email();
        }
        configer.dbConfig FieldToSettings_email()
        {
            configer.dbConfig settings = new configer.dbConfig();
            settings = main.mySettings;

            settings.sender = sender_T.Text;
            settings.mailPassword = Password_T.Password;
            settings.reciever = Reciever_T.Text;
            settings.smtpClient = SMTPClient_T.Text;
            settings.port = StringFormatter.stringToInt(Port_T.Text); ;
            settings.message = Msg_T.Text;
            settings.ssl = SSL_T.IsChecked.Value;
            settings.mailEnabled = Enabled.IsChecked.Value;
            return settings;
        }
        configer.dbConfig SettingsToField_email()
        {
            configer.dbConfig settings = new configer.dbConfig();
            settings = main.mySettings;

            sender_T.Text = settings.sender;
            Password_T.Password = settings.mailPassword;
            Reciever_T.Text = settings.reciever;
            SMTPClient_T.Text = settings.smtpClient;
            Port_T.Text = settings.port.ToString();
            Msg_T.Text = settings.message;
            SSL_T.IsChecked = settings.ssl;
            Enabled.IsChecked = settings.mailEnabled;
            return settings;
        }
        void saveSettings_email()
        {
            var settings = FieldToSettings_email();
            main.mySettings = settings;
        }

        private async void Test_Click(object sender, RoutedEventArgs e)
        {
            saveSettings_email();
            
            await Task.Factory.StartNew(() =>
            {
                try
                {
                    if (main.mySettings.mailEnabled)
                    {
                        mail.send("This is a test email.");
                        r.v("Test email was sent.");
                    }
                    else
                    {
                        r.v("You must check Enabled first.");
                    }

                }
                catch
                {
                    r.v("Test email has failed.");
                }
            });
        }
    }
}
