﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Utilities;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for OptionTab.xaml
    /// </summary>
    public partial class OptionTab : Window
    {
        private void close_Click(object sender, RoutedEventArgs e)
        { 
            anyClose();
        }
        void select()
        {
            main.mySettings.defaultProfile = listBox.SelectedItem.ToString();
            tableName.Text = main.mySettings.defaultProfile;
        }

        void refreshList()
        {
            var sqlConnection = new dbConnection(main.mySettings);
            sqlConnection.checkIfMainExistOrCreate();
            main.mySettings.tables = sqlConnection.getAllTables();
            if (!main.mySettings.tables.Contains(main.mySettings.defaultProfile)) //incase ddefault table does not exist, fall back to main
            {
                main.mySettings.defaultProfile = dbConnection.mainTable;
            }
            listBox.Items.Clear(); ;
            for (int i = 0; i < main.mySettings.tables.Count ; i++)
            {
                listBox.Items.Add(main.mySettings.tables[i]);
            }
            //var mainIndex = 0;
            //var enu = listBox.Items.SourceCollection.GetEnumerator();
            //while (enu.MoveNext())
            //{
            //    if (enu.Current.ToString() == main.mySettings.defaultProfile)
            //    {
            //        break;
            //    }
            //    mainIndex++;
            //}
            listBox.SelectedItem = listBox.Items.GetItemAt(0);
            InitSearcher();
            sqlConnection.disposeConnection();
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            var t = listBox.SelectedItem.ToString();
            if (t == dbConnection.mainTable)
            {
                r.v(dbConnection.mainTable + " cannot be deleted");
                return;
            }
            var response = MessageBox.Show("Delete permanently?", "Delete", MessageBoxButton.YesNo);
            switch (response)
            {
                case MessageBoxResult.None:
                    break;
                case MessageBoxResult.OK:
                    break;
                case MessageBoxResult.Cancel:
                    break;
                case MessageBoxResult.Yes:
                    deleting(t);
                    break;
                case MessageBoxResult.No:
                    break;
                default:
                    break;
            }
            refreshList();
        }

        void deleting(string name)
        {
            deleteTable(name);
            main.mySettings.removeTable(name);
        }

        private void create_Click(object sender, RoutedEventArgs e)
        {
            createButton();
        }

        void createButton()
        {
            var t = textBox.Text;
            if (main.mySettings.tables.Contains(t))
            {
                r.v("Table already exist on the list.");
                return;
            }
            if (StringValidator.OnlyLetterVarify(t) && textBox.Text != dbConnection.mainTable)
            {
                createTable(t);
                r.f(t + " was created.");
                var tables = main.mySettings;
                tables.addTable(t);
                refreshList(); //for search tab
            }
            else
            {
                r.v("New table name must contain only letters, no space, and cannot be main");
            }
        }

        void createTable(string name)
        {
            var c = new dbConnection(main.mySettings);
            c.createTable(name);
            main.mySettings.defaultProfile = name;
            c.disposeConnection();
        }

        void deleteTable(string name)
        {
            var c = new dbConnection(main.mySettings);
            c.deleteTable(name);
            main.mySettings.defaultProfile = dbConnection.mainTable;
            c.disposeConnection();
        }

        private void textBox_GotFocus(object sender, RoutedEventArgs e)
        {
            textBox.Clear();
        }

        private void textBox_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        { 
            if (e.Key == Key.Return)
            {
                createButton();
            }
        }
    }

}