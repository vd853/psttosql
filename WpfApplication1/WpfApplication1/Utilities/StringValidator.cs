﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Utilities
{
    public static class StringValidator
    {
        public static bool OnlyPostiveNegativeNumbers(string input)
        {
            var re = new Regex(@"^\-?[0-9]\d{0,6}$"); //7 digits limit
            return re.IsMatch(input);
        }
        /// <summary>
        /// Verifies if the path is a valid windows path by looking at its format
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool PathVarify(string value)
        {
            Regex r = new Regex(@"^(([a-zA-Z]\:)|(\\))(\\{1}|((\\{1})[^\\]([^/:*?<>""|]*))+)$");
            if (r.IsMatch(value))
            {
                if (Directory.Exists(value))
                {
                    return true;
                }
            }
            return false;
        }
        public static bool OnlyLetterVarify(string value)
        {
            Regex rg = new Regex(@"^[a-zA-Z\w]+$");
            if (rg.IsMatch(value))
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// If search string is keyword. single word on seperated by commas
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool OnlyCommaKeywords(string value)
        {
            var rg = new Regex(@"^(\w+,)+\w+$");
            if (rg.IsMatch(value))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// If string is only numbers up to certain digit length
        /// </summary>
        /// <param name="value"></param>
        /// <param name="digitLimit"></param>
        /// <returns></returns>
        public static bool OnlyNumber(string value, int digitLimit)
        {
            var rg = new Regex(@"^[0-9]{1," + digitLimit.ToString() + "}$");
            if (rg.IsMatch(value))
            {
                return true;
            }
            return false;
        }

        public static bool StringJustNumber(string test) //test if string only contains numbers
        {
            return Regex.IsMatch(test, @"^\d+$");
        }
    }
}
