﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net.Mime;

namespace WpfApplication1.Utilities
{
    static class mail
    {
        public static void send(string sysMessage)
        {
            if (!main.mySettings.mailEnabled) return;
            SmtpClient client = new SmtpClient();
            var from = main.mySettings.sender;
            client.Port = main.mySettings.port;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.Credentials = new System.Net.NetworkCredential(from, main.mySettings.mailPassword);
            client.Host = main.mySettings.smtpClient;
            client.EnableSsl = main.mySettings.ssl;
            MailMessage mm = new MailMessage();
            mm.Attachments.Add(CreateAttachmentFromTxt(fileLog.fullLogPath));
            mm.To.Add(new MailAddress(main.mySettings.reciever));
            mm.From = new MailAddress(from);
            mm.Subject = "PST to MySQL";
            mm.Body = String.Format("Notification: {0} {1} {2} {3} {4} {5} {6}",
                sysMessage,
                Environment.NewLine,
                Environment.NewLine,
                main.mySettings.message,
                Environment.NewLine,
                Environment.NewLine,
                "Session log is attached."
            );

            client.Send(mm);
        }

        static Attachment CreateAttachmentFromTxt(string path)
        {
            // Create  the file attachment for this e-mail message.
            Attachment data = new Attachment(path, MediaTypeNames.Application.Octet);
            // Add time stamp information for the file.
            ContentDisposition disposition = data.ContentDisposition;
            disposition.CreationDate = System.IO.File.GetCreationTime(path);
            disposition.ModificationDate = System.IO.File.GetLastWriteTime(path);
            disposition.ReadDate = System.IO.File.GetLastAccessTime(path);
            // Add the file attachment to this e-mail message.
            return data;
        }
    }
}
