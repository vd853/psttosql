﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;
using System.Reflection;
using Utilities;

namespace WpfApplication1
{
    static partial class configer
    {
        const string saveFile = "config.data";

        public static void save(dbConfig data)
        {
            var s = new FileSerialization(saveFile, dbConfig.newDefault());
            s.save(main.mySettings);
            //BinaryFormatter bf = new BinaryFormatter();
            //FileStream file = File.Create(saveFile);
            //bf.Serialize(file, data);
            //file.Close();
            //r.e("Settings saved");
        }

        public static void SwitchTable(string tableName, bool saveFile = true)
        {
            var con = new dbConnection(main.mySettings);
            if (!con.TableExist(tableName))
            {
                con.createTable(tableName);
                main.mySettings.tables = con.getAllTables();
                main.mySettings.addTable(tableName);
            }
            con.disposeConnection();
            main.mySettings.defaultProfile = tableName;
            if(saveFile) save(main.mySettings);          
        }
        public static dbConfig load()
        {
            var l =  new FileSerialization(saveFile, dbConfig.newDefault());
            var loaded = l.load() as dbConfig;
            consoleSettings(loaded);
            return loaded;
            //dbConfig config = new dbConfig();
            //if (File.Exists(saveFile))
            //{
            //    BinaryFormatter bf = new BinaryFormatter();
            //    FileStream file = File.Open(saveFile, FileMode.Open);
            //    config = (dbConfig)bf.Deserialize(file);
            //    file.Close();
            //    consoleSettings(config);
            //    return config;
            //}
            //else
            //{
            //    config = dbConfig.newDefault();
            //    save(config);
            //    main.mySettings = config;

            //    return config;
            //}
            
        }
        static void consoleSettings(dbConfig settings)
        {
            r.e("SETTINGS SERVER: " + settings.server);
            r.e("SETTINGS USER: " + settings.user);
            r.e("SETTINGS PROFILE: " + settings.defaultProfile);
            r.e("SETTINGS PASS: " + settings.password);
            r.e("SETTINGS PATH: " + settings.searchPath);
            r.e("SETTINGS MULTITHREAD: " + settings.multithread.ToString());
            r.e("PROFILES FOUND: " + settings.tables.Count);
        }
    }
}
