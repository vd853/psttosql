﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1
{
    static partial class configer
    {
        [Serializable]
        public class dbConfig
        {
            public string server;
            public string defaultProfile;
            public string user;
            public string password;
            public bool multithread;
            public int maxLog;
            private bool searchTableUnConfig;
            public int tableBuffer;
            public int viewLimit;
            public string db;
            public bool importSplit;
            public bool mustExist;
            public string searchPath;

            public List<string> tables; //only for index reference, use addtable() for adding tables
            private List<string> searchTables;

            //mail
            public string sender;
            public string mailPassword;
            public string reciever;
            public string smtpClient;
            public int port;
            public string message;
            public bool ssl;
            public bool mailEnabled;
            public bool trayMinimize;

            public dbConfig()
            {
                tableBuffer = 5;
                viewLimit = 200;
                maxLog = 100;
                db = "main";
                server = "localhost";
                defaultProfile = "main";
                user = "datauser";
                password = "User1";
                importSplit = true;
                searchPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                tables = new List<string>();
                tables.Add("main");
                searchTables = new List<string>(tables);
                multithread = true;
                sender = "viluan@fensterstock.com";
                mailPassword = "12345";
                reciever = "viluan@fensterstock.com";
                smtpClient = "mail.fensterstock.com";
                port = 25;
                ssl = false;
                message = "";
                mailEnabled = false;
                searchTableUnConfig = true;
                mustExist = true;
                trayMinimize = false;
            }

            public static dbConfig newDefault()
            {

                return new dbConfig();
            }

            public void resetTables() //use right after all tables are deleted
            {
                searchTables = new List<string>();
                tables = new List<string>();
                searchTables.Add("main");
                tables.Add("main");
            }
            public List<string> getSearchTable()
            {
                CleanSearchTable();
                return searchTables;
            }

            public List<string> getNoSearchTable()
            {
                return new List<string>(tables.Except(searchTables));
            }

            public void addTable(string name)
            {
                if (!tables.Contains(name))
                {
                    tables.Add(name);
                }
                //if (!searchTables.Contains(name)) //new table will be included in search
                //{
                //    searchTables.Add(name);
                //}
            }

            public int tableCount()
            {
                return tables.Count;
            }
            public void removeTable(string name)
            {
                tables.Remove(name);
                searchTables.Remove(name);
            }
            void CleanSearchTable()
            {
                var remove = new List<string>(searchTables.Except(tables));
                foreach (var e in remove)
                {
                    searchTables.Remove(e);
                }
            }
            public void setSearchTables(List<string> setter)
            {
                searchTables = new List<string>(setter);
            }
        }
    }
}
