﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Utilities
{
    public static class StringFormatter
    {
        /// <summary>
        /// Trims a string to certain length and removes empty spaces or line breaks
        /// </summary>
        /// <param name="text"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public static string WordsOnly(string text, int limit = -1)
        {
            var reword = new string(text.Where(char.IsLetter).ToArray()); //keep only letters
            reword = Regex.Replace(reword, @" ", ""); //removes spaces
            reword = Regex.Replace(reword, Environment.NewLine, ""); //remove line breaks
            if (limit > 0)
            {
                if (reword.Length > limit)
                {
                    reword = reword.Substring(0, limit);
                }
            }
            return reword;
        }

        /// <summary>
        /// takes an int an words it into a string like 101 will be onehundredandone
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static string NumberToWords(int number)
        {
            if (number == 0)
                return "zero";

            if (number < 0)
                return "minus " + NumberToWords(Math.Abs(number));

            string words = "";

            if ((number / 1000000) > 0)
            {
                words += NumberToWords(number / 1000000) + " million ";
                number %= 1000000;
            }

            if ((number / 1000) > 0)
            {
                words += NumberToWords(number / 1000) + " thousand ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += NumberToWords(number / 100) + " hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != "")
                    words += "and ";

                var unitsMap = new[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
                var tensMap = new[] { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += "-" + unitsMap[number % 10];
                }
            }

            return words;
        }

        /// <summary>
        /// Looks through a string and removes any duplicate words. Returns a string
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string dupeString(string text) //deduplicate words in string
        {
            var splitted = text.Split(' ');
            var deduplicateEnum = splitted.Distinct();
            string[] deduplicateArray = deduplicateEnum.Cast<string>().ToArray();
            var deduplicateString = String.Join(" ", deduplicateArray);
            return deduplicateString;
        }

        /// <summary>
        /// Removes weird char and extra spaces
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string CleanString(string text)
        {
            StringBuilder temp0 = new StringBuilder(text);
            var temp = temp0.ToString();
            temp = Regex.Replace(temp, @"[^\u0000-\u007F]", "?"); //remove werid char
            temp = Regex.Replace(temp, @"\s+", " "); //remove extra spaces
            return temp;
        }

        public static int stringToInt(string value)
        {
            return Convert.ToInt32(value);
        }
    }   
}
