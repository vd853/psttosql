﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Runtime.InteropServices;

using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Threading;
using Utilities;

namespace WpfApplication1
{
    class dbConnection
    {    
        private MySqlConnection connection;
        private string server;
        private string database;
        private string uid;
        private string password;
        public static bool cancelled;
        public const string mainTable = "main";
        public const string maindb = "main";

        //Constructor
        public dbConnection(configer.dbConfig mySettings)
        {
            Initialize(mySettings);
        }

        //Initialize values
        private void Initialize(configer.dbConfig mySettings)
        {
            server = mySettings.server; // "localhost";
            database = mySettings.db; // "main";
            uid = mySettings.user; // "datauser";
            password = mySettings.password; // "User1";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";" + "Connection Timeout=6;";
            connection = new MySqlConnection(connectionString);
            //r.f("connecting with table: " + database);
            if(!MainWindow.isRelease) main.currentOpen++;
            //if (!MainWindow.isRelease) r.f("current Open: " + main.currentOpen);
        }

        public static void ResetPool(int delay = 0)
        {
            main.currentPooling = 0;
            if(delay != 0) Thread.Sleep(delay);
            var con = new dbConnection(main.mySettings);
            con.ClearPool();
            con.disposeConnection();
        }

        public void updateAllTables()
        {         
            var tables = getAllTables();
            if (!tables.Contains(main.mySettings.defaultProfile))
            {
                main.mySettings.defaultProfile = mainTable;
            }
        }
        //open connection to database
        public void disposeConnection()
        {
            if (!MainWindow.isRelease) main.currentOpen--;
            //await connection.ClearPoolAsync(connection);
            connection.Dispose();
        }

        public void ClearPool()
        {
            connection.ClearAllPoolsAsync();
        }
        private void OpenConnection(bool halt = true)
        {
            if (!halt)
            {
                connection.Open();
                return;
            }

            try
            {
                connection.Open();
            }
            catch
            {
                ConnectionCheckAndPause("OpenConnection");
            }
            
            
        }

        private async void ConnectionCheckAndPause(string processType)
        {
            if (connection.State != ConnectionState.Open)
            {
                fileLog.outlog("Openning connection has failed during " + processType + ". Retrying...", true);
                await Task.Run(() =>
                {
                    while (connection.State != ConnectionState.Open)
                    {
                        Task.Delay(5000);
                        try
                        {
                            connection.Open();
                        }
                        catch
                        {

                        }
                    }
                });
            }
        }

        public bool checkConnection()
        {
            var c = new console("Connection Test", ConsoleMode.smalllog);
            c.log("Testing Connection...");
            c.Show();
            try
            {
                connection.Open();
                connection.Close();
                r.f("Connection test successful");
                main.connectionStatus = true;
                c.Close();
                return true;
            }
            catch
            {
                main.connectionStatus = false;
                r.f("Connection has failed");
                c.Close();
                return false;
            } 
        }

        public bool CheckConnectionQuick()
        {
            try
            {
                connection.Open();
                connection.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }

        //Close connection
        private void CloseConnection()
        {
            connection.Close();
        }

        //Insert statement
        public void InsertMailData(string[] queries)
        {
            //r.f("Importing folder items");
            if (main.cancelled)
            {
                r.f("Insertion is cancelled");
                return;
            }

            cancelled = false;
            MySqlCommand cmd;
            //open connection 
            //create command and assign the query and connection from the constructor
            OpenConnection();
            foreach (var q in queries)
            {
                //r.f("INSERT: " + q);
                cmd = new MySqlCommand(q, connection);
                cmd.ExecuteNonQuery();
                //try
                //{

                //}
                //catch
                //{
                //    r.f("error inserting => " + q + " <=");
                //}
                cmd.Dispose();
                if (cancelled)
                {
                    r.f("Insertion was just cancelled");
                    break;
                }
            }
            CloseConnection();
        }

        public void InsertMailDataSingle(string query)
        {
            if (query == "") return;
            querySQL(query);
        }

        public DataTable getDataTableX(string search, out int rowCount) //not used
        {
            //Console.WriteLine("Query search: " + search);
            //string searcher = @"select received, subject, body, recipient, folderName, fileName, entryID from "+ main.mySettings.database + @" where body like " + fixSearch(search) + " OR subject like " + fixSearch(search) + " order by received desc";
            string searcher = @"select received, subject, body, recipient, folderName, fileName, entryID from " + main.mySettings.defaultProfile + @" where body regexp " + search + " OR subject regexp " + search + " OR recipient regexp " + search + " order by received desc";
            //Console.WriteLine("searcher: " + searcher);
            var dt = querySQLTable(searcher);
            rowCount = dt.Rows.Count;
            return dt;
        }

        public DataTable getDataTableLimited(string search, int startRow, int nextNRows)
        {
            //Console.WriteLine("Query search: " + search);
            search = fixSearchRegex(search);
            //r.v("Search " + search);
            string searcher = @"select received, subject, body, recipient, folderName, fileName, entryID from " + main.mySettings.defaultProfile + @" where body regexp " + search + " OR subject regexp " + search + " OR recipient regexp " + search + " order by received desc" + " limit " + startRow.ToString() + ", " + nextNRows.ToString();
            //Console.WriteLine("searcher: " + searcher);
            var dt = querySQLTable(searcher);
            return dt;
        }

        public DataTable getUnionDataTableLimited(string search, int startRow, int nextNRows)
        {   
            //Console.WriteLine("searcher: " + searcher);
            var dt = querySQLTable(GetSearchQueryString(search, startRow, nextNRows));
            return dt;
        }

        public string GetSearchQueryString(string search, int startRow, int nextNRows)
        {
            var AndCondition = main.mySettings.mustExist;
            //Console.WriteLine("Query search: " + search);
            var fullQuery = new StringBuilder();
            var tables = main.mySettings.getSearchTable();
            search = fixSearchRegex(search);
            //r.v("Search " + search);
            var trailAppend = " order by received desc" + " limit " + startRow.ToString() + ", " + nextNRows.ToString();
            //string searcher = @"select received, subject, body, recipient, folderName, fileName, entryID from " + main.mySettings.database + @" where body regexp " + search + " OR subject regexp " + search + " OR recipient regexp " + search;
            for (int i = 0; i < tables.Count; i++)
            {
                if (!AndCondition)
                {
                    fullQuery.Append(
                        @"select received, SUBSTRING(subject, 1, 50) as subject, SUBSTRING(body, 1, 150) as body, recipient, folderName, fileName, entryID from " +
                        tables[i] + @" where concat(subject, body, recipient) regexp " + search);
                }
                else
                {
                    fullQuery.Append(
                        @"select received, SUBSTRING(subject, 1, 50) as subject, SUBSTRING(body, 1, 150) as body, recipient, folderName, fileName, entryID from " +
                        tables[i] + @" where body regexp " + search + " AND subject regexp " + search);
                }
                
                if (i != tables.Count - 1)
                {
                    fullQuery.Append(" union ");
                }
            }
            fullQuery.Append(trailAppend);

            return fullQuery.ToString();
        }

        public int getUnionDataTableCount(string search, out string restringSearch)
        {
            var AndCondition = main.mySettings.mustExist;
            //Console.WriteLine("Query search: " + search);
            restringSearch = fixSearchRegexNoLines(search);
            var tables = main.mySettings.getSearchTable();
            var fullQuery = new StringBuilder();
            search = fixSearchRegex(search);
            //string searcher = @"select count(*) from " + main.mySettings.database + @" where body regexp " + search + " OR subject regexp " + search + " OR recipient regexp " + search;
            for (int i = 0; i < tables.Count; i++)
            {
                if (!AndCondition)
                {
                    fullQuery.Append(@"select count(*) from " + tables[i] +
                                     @" where concat(subject, body, recipient) regexp " + search);
                }
                else
                {
                    fullQuery.Append(@"select count(*) from " + tables[i] +
                                     @" where body regexp " + search + " AND subject regexp " + search);
                }
               
                if (i != tables.Count - 1)
                {
                    fullQuery.Append(" union ");
                }
            }
            return IntQueryReaderMultiple(fullQuery.ToString());
        }

        private string fixSearch(string search)
        {
            // creates this "%mysearch%"
            return @"""%" + search + @"%""";
        }

        private static string fixSearchRegex(string search)
        {
            StringBuilder temp0 = new StringBuilder(search);
            var temp = temp0.ToString();
            temp = temp.Replace(",", "|"); //add | for regex multikeyword
            temp = Regex.Replace(temp, @"\s+", ""); //remove extra spaces
            temp = "'" + temp + "'"; //add quotes
            //r.v(temp);
            return temp;
        }

        private string fixSearchRegexNoLines(string search) //use to make title text for grid window
        {
            StringBuilder temp0 = new StringBuilder(search);
            var temp = temp0.ToString();
            temp = Regex.Replace(temp, @"\s+", ""); //remove extra spaces
            temp = "'" + temp + "'"; //add quotes
            //r.v(temp);
            return temp;
        }


        //result from a single 
        public class streamData: EventArgs, getMails.ImailData
        {
            public string body { get; set; }

            public DateTime date { get; set; }

            //order received, subject, body, recipient, folderName, fileName, entryID


            public string dateText { get; set; }

            public string entryID { get; set; }

            public string fileName { get; set; }

            public string folderName { get; set; }

            public string mailIndex { get; set; }

            public string recipient { get; set; }

            public string subject { get; set; }

            public string text { get; set; } //test

            public bool completed { get; set; }
        }
        public event EventHandler<streamData> QueryStreamGot;
        protected virtual void OnQueryStreamGot(streamData e)
        {
            if (QueryStreamGot != null)
            {
                QueryStreamGot(this, e);
            }
        }

        bool breakQueryStream;
        public void queryStream(string query)
        {
            breakQueryStream = false;
            OpenConnection();
            var command = new MySqlCommand();
            command.CommandText = query;
            command.Connection = connection;
            r.f(query);
            try
            {
                var result = command.ExecuteReader();
                while (result.Read())
                {
                    //order: received, subject, body, recipient, folderName, fileName, entryID
                    if (breakQueryStream)
                    {
                        result.Close();
                        r.f("streambroken");
                    }
                    else
                    {
                        OnQueryStreamGot(new streamData()
                        {
                            dateText = result[0].ToString(),
                            subject = result[1].ToString(),
                            body = result[2].ToString(),
                            recipient = result[3].ToString(),
                            folderName = result[4].ToString(),
                            fileName = result[5].ToString(),
                            entryID = result[6].ToString(),
                            completed = false
                        });
                    }
                    //Thread.Sleep(500);
                }
            }
            catch
            {
                fileLog.outlog("Attempting to read sql after closing!", true);
            }
            if (!breakQueryStream) //notify the gridwindow that all stream is completed
            {
                OnQueryStreamGot(new streamData()
                {
                    completed = true
                });
            }
            CloseConnection();
        }
        public void queryStreamBreaker() { breakQueryStream = true; }

        List<string> getSingleColumn(string query)
        {
            OpenConnection();
            var itemsRetrived = new List<string>();
            var command = new MySqlCommand();
            command.CommandText = query;
            command.Connection = connection;
            var result = command.ExecuteReader();
            while (result.Read())
            {
                itemsRetrived.Add(result[0].ToString());
            }
            CloseConnection();
            return itemsRetrived;
            
        }

        public List<string> getAllTables()
        {
            var tableList = getSingleColumn("SHOW TABLES");
            tableList.RemoveAll(x => x.Contains("migrationhistory"));
            tableList.Sort();
            return tableList;
        }

        public int getTableEntryCount(string table = "-1")
        {
            if (table == "-1")
            {
                table = main.mySettings.defaultProfile;
            }
            string query = @"select count(*) from " + table;
            return queryScaler(query);
        }

        int queryScaler(string query)
        {
            int result;
            OpenConnection();
            using (MySqlCommand command = new MySqlCommand())
            {
                command.CommandText = query;
                command.Connection = connection;
                result = 0;
                try
                {
                    result = StringFormatter.stringToInt(command.ExecuteScalar().ToString());
                }
                catch
                {
                    ConnectionCheckAndPause("queryScaler");
                }
                
            }        
            CloseConnection();
            return result;
        }

        int IntQueryReaderMultiple(string query)
        {
            var sum = new List<int>();
            OpenConnection();
            MySqlCommand command = new MySqlCommand();
            command.CommandText = query;
            command.Connection = connection;
            var s = command.ExecuteReader();
            while (s.Read())
            {
                sum.Add(StringFormatter.stringToInt(s.GetValue(0).ToString()));
            }
            CloseConnection();
            var allSum = 0;
            foreach (int i in sum)
            {
                allSum += i;
            }
            return allSum;
        } 
        private DataTable querySQLTable(string query)
        {
            OpenConnection();
            MySqlCommand command = new MySqlCommand();
            command.CommandText = query;
            command.Connection = connection;
            MySqlDataAdapter adapt = new MySqlDataAdapter(command);
            DataTable data = new DataTable();
            adapt.Fill(data);
            CloseConnection();
            return data;
        }

        public bool TableExist(string tableName)
        {
            var tables = getAllTables();
            if (tables.Contains(tableName))
            {
                return true;
            }
            return false;
        }
        public void createTable(string tableName)
        {
            var creator = new StringBuilder();
            creator.Append(@"CREATE TABLE `");
            creator.Append(tableName);

            //show create table main;
            //you must copy from ` to ; exactly, note ` not the same as ' 
            creator.Append(@"` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fileName` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  `folderName` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `received` datetime DEFAULT NULL,
  `subject` longtext CHARACTER SET utf8,
  `body` longtext,
  `entryID` varchar(128) NOT NULL,
  `recipient` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `entryID_UNIQUE` (`entryID`),
  KEY `idx_main_entryID` (`entryID`)
) ENGINE=InnoDB AUTO_INCREMENT=18244 DEFAULT CHARSET=eucjpms;");

            querySQL(creator.ToString());
            querySQL(@"drop index idx_main_entryID on " + tableName);
            querySQL(@"create index idx_main_entryID on " + tableName + @" (entryID)");
        }

        /// <summary>
        /// No return query, use for single line insertion too
        /// </summary>
        /// <param name="query"></param>
        private bool querySQL(string query, bool logError = true)
        {
            var result = false;
            OpenConnection();
            using (var command = new MySqlCommand())
            {
                command.CommandText = query;
                command.Connection = connection;
                try
                {
                    command.ExecuteNonQuery();
                    result = true;
                }
                catch
                {
                    try
                    {
                        Thread.Sleep(2000);
                        ConnectionCheckAndPause("querySQL"); //check for disconnection
                        command.ExecuteNonQuery();
                        result = true;
                    }
                    catch
                    {
                        //var queryTruncated = r.TruncateString(query, 200);
                        if(logError) fileLog.outlog("Query Error: " + query, true);
                        ConnectionCheckAndPause("querySQL"); //check for disconnection
                    }
                }
            }
            CloseConnection();
            return result;
        }

        public void CheckDeleteEmptyTable(string table)
        {
            if (getTableEntryCount(table) <= 0)
            {
                deleteTable(table);
            }
        }

        public void deleteAllTable() 
        {
            var toDelete = getAllTables().Except(new List<string>() { mainTable });
            foreach (var s in toDelete)
            {
                deleteTable(s);
            }
            recreateTableMain();
            main.mySettings.resetTables();
        }

        public bool createDatabase(string databaseName)
        {
            return querySQL(@"create schema " + databaseName);
        }
        public bool dropDatabase(string databaseName)
        {
            return querySQL(@"drop schema " + databaseName);
        }
        public void deleteTable(string tableName)
        {
            querySQL(@"DROP TABLE " + tableName);
        }
        public void recreateTable(string useTable = "")
        {
            if (useTable == "") useTable = main.mySettings.defaultProfile;
            querySQL(@"DROP TABLE " + useTable);
            createTable(useTable);
        }
        public void recreateTableMain()
        {
            querySQL(@"DROP TABLE " + mainTable);
            createTable(mainTable);
        }

        public void checkIfMainExistOrCreate()
        {
            if (!getAllTables().Contains(mainTable))
            {
                recreateTable(mainTable);
            }
        }
        public void optimizeTable()
        {
            createIndex();
            foreach (var table in getAllTables())
            {
                querySQL(@"alter table " + table);
                querySQL(@"analyze table " + table);
                querySQL(@"optimize table " + table);
            }   
        }

        void createIndex()
        {
            var prefix = " index idx_";
            StringBuilder query;
            foreach (var table in getAllTables())
            {
                query = new StringBuilder();
                query.Append("drop");
                query.Append(prefix);
                query.Append(table);
                query.Append("_entryID");
                query.Append(" on ");
                query.Append(table);
                querySQL(query.ToString(), false);

                query = new StringBuilder();
                query.Append("create");
                query.Append(prefix);
                query.Append(table);
                query.Append("_entryID");
                query.Append(" on ");
                query.Append(table);
                query.Append(" (entryID)");
                querySQL(query.ToString(), false);
                /////////////////////////
                query = new StringBuilder();
                query.Append("drop");
                query.Append(prefix);
                query.Append(table);
                query.Append("_subject");
                query.Append(" on ");
                query.Append(table);
                querySQL(query.ToString(), false);

                query = new StringBuilder();
                query.Append("create");
                query.Append(prefix);
                query.Append(table);
                query.Append("_subject");
                query.Append(" on ");
                query.Append(table);
                query.Append(" (subject(1024))");
                querySQL(query.ToString(), false);
                /////////////////////////
                query = new StringBuilder();
                query.Append("drop");
                query.Append(prefix);
                query.Append(table);
                query.Append("_body");
                query.Append(" on ");
                query.Append(table);
                querySQL(query.ToString(), false);

                query = new StringBuilder();
                query.Append("create");
                query.Append(prefix);
                query.Append(table);
                query.Append("_body");
                query.Append(" on ");
                query.Append(table);
                query.Append(" (body(1024))");
                querySQL(query.ToString(), false);
            }
        }
    }

}
