﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Diagnostics;
using System.Text.RegularExpressions;
using Utilities;

namespace WpfApplication1
{
    class dbMail
    {
        List<getMails.mailData> collection;

        public dbMail(List<getMails.mailData> data)
        {
            collection = data;
        }

        /// <summary>
        /// Builds muliple queries in form of strings based on the collection variable
        /// </summary>
        /// <returns></returns>
        public string[] getQueries()
        {
            string[] queries = new string[collection.Count];
            for (int i = 0; i < collection.Count; i++)
            {
                queries[i] = buildQuery(collection[i]);
            }
            return queries;
        }

        /// <summary>
        /// Convert a single mailData into a single query
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string buildQuery(getMails.mailData data)
        {
            if (data.entryID == "ENTRY ID ERROR")
            {
                return "";
            }
            StringBuilder query = new StringBuilder();
            query.Append("INSERT IGNORE INTO " + main.mySettings.defaultProfile + @" (fileName, folderName, received, subject, entryID, recipient, body) VALUES ('");

            //clean string required for folderName, recipient, subject, body
            query.Append(subQuery(data.fileName));
            query.Append(subQuery(cleanString(data.folderName)));
            query.Append(subQuery(formatDate(data.date)));
            query.Append(subQuery(cleanString(data.subject)));
            query.Append(subQuery(data.entryID));
            query.Append(subQuery(cleanString(data.recipient)));
            query.Append(cleanString(data.body) + "')"); //Last query and closing

            return query.ToString();
        }


        //Below are query formatters
        static string subQuery(string subData)
        {
            return subData + @"','";
        }

        public static string formatDate(DateTime input)
        {
            return input.ToString("yyyy-MM-dd HH:mm");
        }

        public static string cleanString(string input)
        {
            StringBuilder temp0 = new StringBuilder(input);
            var temp = temp0.ToString();
            temp = StringFormatter.CleanString(temp);                  
            temp = Regex.Replace(temp, @"[^?a-zA-Z0-9 -]", " "); //only keep alphanumeric and ?
            temp = temp.Replace(Environment.NewLine, " "); //removes line break with single space 
            temp = StringFormatter.dupeString(temp);
            temp = temp.ToLower();
            return temp;
        }
    }
}
