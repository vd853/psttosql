﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Text.RegularExpressions;
using Utilities;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static MainWindow w;
        public static string[] pstPaths, pstSelectedPaths;
        static getPST getPst; //contains the profile information
        private static List<getPST.PSTProfileModel> revertablePSTProfile;
        bool pstFound; //this is use at getPaths()
        private static List<string> completedPST;

        Stopwatch sw;
        BackgroundWorker worker, worker2; //this is the importer
        bool ranOnce; //will be true if run at least once
        private bool keywordUsed;
        private bool discovering;

        Task entryUpdater;
        CancellationTokenSource entryUpdaterS;
        CancellationToken entryUpdaterCT;

        public static bool isRelease = true; //set true for non-debug builds also set back to windows application

        public MainWindow()
        { 
            InitializeComponent();  
            //debug controllers
            if (!isRelease)
            {
                debugItems(true);
            }
            else
            {
                debugItems(false);
            }
            w = this;
            textBox.Clear();
            log("===== Application Started =====");
            main.init();
            if (!main.connectionStatus) Environment.Exit(0);
            discover.IsEnabled = false;
            path.Text = main.mySettings.searchPath;
            mtCheckBox.IsChecked = main.mySettings.multithread;
            mtCheckboxChecker();

            //can delay 
            sw = new Stopwatch();
            lockWhileImporting(false);
            showDetail.Content = "Show Details";
            andSearch.IsChecked = main.mySettings.mustExist;
            if (main.mySettings.trayMinimize)
            {
                MinimizeToTray.Enable(this);
            }
        }

        void debugItems(bool isVisible)
        {
            showDetail.Visibility = r.SetVisibility(isVisible);
            mtCheckBox.Visibility = r.SetVisibility(isVisible);
            //L1.Visibility = r.SetVisibility(isVisible);
            //L2.Visibility = r.SetVisibility(isVisible);
            //L3.Visibility = r.SetVisibility(isVisible);
            //L1_import.Visibility = r.SetVisibility(isVisible);
            //L2_entry.Visibility = r.SetVisibility(isVisible);
            //L3_throttled.Visibility = r.SetVisibility(isVisible);

        }
        void lockWhileImporting(bool isLocked)
        {
            //active while running
            if (!ranOnce) //becomes visible when first import is run
            {
                if (!isRelease) //not visible for release build
                {
                    //put non visible items here, currently all is visible
                }
                L1.Visibility = r.SetVisibility(isLocked);
                L2.Visibility = r.SetVisibility(isLocked);
                L1_import.Visibility = r.SetVisibility(isLocked);
                L2_entry.Visibility = r.SetVisibility(isLocked);
                L3.Visibility = r.SetVisibility(isLocked);
                L3_throttled.Visibility = r.SetVisibility(isLocked);
            }
            cancel.IsEnabled = isLocked;

            //not active while running
            Option.IsEnabled = !isLocked;
            path.IsEnabled = !isLocked;
            //searchButton.IsEnabled = !isLocked;
            //search.IsEnabled = !isLocked;
            discover.IsEnabled = !isLocked;
            Import.IsEnabled = !isLocked;
            mtCheckBox.IsEnabled = !isLocked;
        }
        //this is called after the showdialog
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            validatePath();
            //var fs = new FileSelector(pstPaths);
            //fs.ShowDialog();
        }

        //loggers
        public void log(string text)
        {
            textBox.Text += text + Environment.NewLine;
            if(textBox.LineCount > 500)
            {
                textBox.Clear();
            }
            textBox.ScrollToEnd();
        }
        public void ilog(string text)
        {
            if (!main.cancelled)
            {
                log(text);
            }
        }

        //searchers
        private void searchButton_Click(object sender, RoutedEventArgs e)
        {
            var status = new main.throttleState(main.mySettings.server);
            if (status.isThrottleConnection())
            {
                r.v("Cannot connect to database.");
                return;
            }
            showSearch();
        }

        void showSearch()
        {
            var t = Regex.Replace(search.Text, " ", "");
            if (StringValidator.OnlyCommaKeywords(t) || StringValidator.OnlyLetterVarify(t)) //second condition is to verify single keyword
            {
                r.e("Searching for " + search.Text);
                var dg = new GridWindow(t);
                dg.Show();
            }
            else
            {
                r.e("Invalid Search Keyword. Usage: apple, dog");
                search.Text = "";
            }
        }

        private void search_GotFocus(object sender, RoutedEventArgs e)
        {
            if (!keywordUsed)
            {
                keywordUsed = true;
                r.f("focused on search");
                search.Text = "";
            }
        }

        private void search_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                showSearch();
            }
        }

        //discovers
        private void path_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                validatePath(true);
            }
        }
        private void discover_Click(object sender, RoutedEventArgs e)
        {
            validatePath(true);
        }
        public async void getPaths()
        {
            var pa = path.Text;
            if (discovering)
            {
                r.v("Please wait for PST discovery.");
                return;
            }
            var msg = "Discovering PST files....";
            log(msg);
            discovering = true;
            getPst = await Task.Run(()=> { return new getPST(pa); });
            discovering = false;
            var pstProfile = getPst.PSTProfile;
            pstPaths = getPst.getAllPST();
            if (pstPaths.Length == 0)
            {
                r.e("No PST found.");
            }
            else
            {
                pstFound = true;
                var pstFoundmsg = pstPaths.Length + " PST found: ";
                fileLog.outlog(msg, false);
                fileLog.outlog(pstFoundmsg, false);
                foreach (var s in pstProfile)
                {
                    r.e("Profile " + s.profileName + " for File: " + s.fileLocation);
                    fileLog.outlog(s.fileLocation, false);
                }
                r.e(pstFoundmsg);
                if (!main.mySettings.importSplit)
                {
                    r.e("Since [Option > Import split by folder] is disabled");
                    r.e("All imports will go into profile: " + main.mySettings.defaultProfile);
                }
            }
        }
        
        private void path_LostFocus(object sender, RoutedEventArgs e)
        {
            validatePath(true);
        }
        bool validatePath(bool save = false)
        {
            if (!StringValidator.PathVarify(path.Text))
            {
                r.e("Path is invalid or does not exist.");
                discover.IsEnabled = false;
                return false;
            }else
            {
                if (save)
                {
                    main.mySettings.searchPath = path.Text;
                    configer.save(main.mySettings);
                }    
                discover.IsEnabled = true;
                getPaths();
                return true;
            }
        }
        
        
        //MISC buttons

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            OutlookManager.closeAllStores();
            fileLog.outlog("END", false);
            Close();
            Environment.Exit(0);
        }

        private void Option_Click(object sender, RoutedEventArgs e)
        {
            var o = new OptionTab();
            o.ShowDialog();
        }

        private void mtCheckBox_Click(object sender, RoutedEventArgs e)
        {
            mtCheckboxChecker();
        }

        private void andSearch_Click(object sender, RoutedEventArgs e)
        {
            main.mySettings.mustExist = andSearch.IsChecked.Value;
            configer.save((main.mySettings));
        }

        private void showlog_Click(object sender, RoutedEventArgs e)
        {
            var c = new console("File log: " + fileLog.fullLogPath, ConsoleMode.log);
            c.logList(fileLog.getLogFileListForm());
            c.Show();
        }

        void mtCheckboxChecker()
        {
            main.mySettings.multithread = mtCheckBox.IsChecked.Value;
            configer.save(main.mySettings);
        }

        private void showDetail_Click(object sender, RoutedEventArgs e)
        {
            if(showDetail.Content.ToString() == "Show Details")
            {
                consoleController.showConsole();
                showDetail.Content = "Hide Details";
            }else
            {
                consoleController.hideConsole();
                showDetail.Content = "Show Details";
            }
        }
    }


}
