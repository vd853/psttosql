﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for console.xaml
    /// </summary>
    public enum ConsoleMode
    {
        quick,
        log,
        smalllog
    }; 
    public partial class console : Window
    {
        bool secondDetection;
        private bool isClosing;
        private ConsoleMode consoleM;
        public console(string myTitle, ConsoleMode cm)
        {
            InitializeComponent();
            Title = myTitle;
            consoleM = cm;
            switch (cm)
            {
                case ConsoleMode.quick:
                    Height = 200;
                    Width = 320;
                    window.WindowStyle = WindowStyle.None;
                    break;
                case ConsoleMode.log:
                    Height = System.Windows.SystemParameters.PrimaryScreenHeight * 0.80;
                    Width = System.Windows.SystemParameters.PrimaryScreenWidth * 0.8;
                    window.WindowStyle = WindowStyle.SingleBorderWindow;
                    break;
                case ConsoleMode.smalllog:
                    Height = 290;
                    Width = 260;
                    window.WindowStyle = WindowStyle.None;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(cm), cm, null);
            } 
            textBox.Clear();
        }

        public void setLocation(int x, int y)
        {
            window.Left = x-100;
            window.Top = y-100;
        }
        public void log(string text)
        {
            textBox.Text = text;
        }

        public void logList(List<string> message)
        {
            for (int i = 0; i < message.Count; i++)
            {
                textBox.Text += message[i] + Environment.NewLine;
            }
            textBox.ScrollToEnd();
        }

        private void window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void close_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        void QuickClose()
        {
            if (!isClosing)
            {
                if (consoleM == ConsoleMode.quick)
                {
                    Close();
                }
                isClosing = true;
            }
            
        }

        private void window_MouseMove(object sender, MouseEventArgs e)
        {
            if (!secondDetection)
            {
                secondDetection = true;
            }
            else
            {
                QuickClose();
            }
            
        }
    }
}
