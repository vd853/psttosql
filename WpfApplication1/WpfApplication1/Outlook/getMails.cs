﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.Office.Interop.Outlook;
using System.Text.RegularExpressions;

namespace WpfApplication1
{
    static class getMailWorkers
    {
        public static List<Task> importers = new List<Task>();
        public static int getActive()
        {
            var count = 0;
            foreach (Task item in importers)
            {
                if (!item.IsCompleted)
                {
                    count++;
                }
            }
            return count;
        }
    }
    class getMails
    {
        public interface ImailData
        {
            string recipient { get; set; }
            string fileName { get; set; }
            string folderName { get; set; }
            DateTime date { get; set; }
            string subject { get; set; }
            string body { get; set; }
            string entryID { get; set; }
        }
        public class mailData : ImailData
        {
            public string recipient { get; set; }
            public string fileName { get; set; }
            public string folderName { get; set; }
            public DateTime date { get; set; }
            public string subject { get; set; }
            public string body { get; set; }
            public string entryID { get; set; }
        }
        private List<mailData> collection;
        private string pstLocation;
        //private NameSpace outlookNs;
        //private MAPIFolder rootFolder;
        private OutlookManagerI om;
        public enum getMode { getFolderSingle, getFolder, openMail, verify, debug };
        public bool actionCancelled;
        public bool isDirect = false; //when mail item is retreived, query will be execute for that single item
        private string pstPathReference;
        public bool storeIsOpenAble; //use for inital varification before import
        public getMails(string pstPath, getMode mode, string entryID = "")
        {
            pstPathReference = pstPath;
            if (!r.fileExist(pstPath))
            {
                if (mode == getMode.openMail)
                {
                    r.v("[" + pstPath + "] must be accessible locally.");
                }   
                fileLog.outlog("PST cannot be found. from " + pstPath, true);
                return;
            }
            pstLocation = pstPath;
            if (String.IsNullOrWhiteSpace(pstLocation))
            {
                r.f("PST path is invalid");
                return;
            }
            collection = new List<mailData>();
            var app = new Application();   
            var outlookNs = app.GetNamespace("MAPI");
            //check if pst can be open, otherwise log and exit this pst openning
            try
            {
                outlookNs.AddStore(pstPath);
                storeIsOpenAble = true;
            }
            catch
            {
                fileLog.outlog("Error openning " + pstPath, true);
            }
            if (!storeIsOpenAble) return;

            om = new OutlookManagerI();
            var storeName = om.getStoreName(pstPath); //must be added before it can be found
            if (storeName == "null")
            {
                actionCancelled = true;
                return;
            }
            actionCancelled = false;
            var rootFolder = outlookNs.Stores[storeName].GetRootFolder();
            OutlookManager.addStore(pstPath);
            switch (mode)
            {
                case getMode.getFolderSingle:
                    isDirect = true;
                    //r.f("Openning direct: " + pstPath);
                    try //will try this twice
                    {
                        if (main.cancelled)
                        {
                            r.f("Cancelled Root Mail Folder Retrival.");
                            return;
                        }
                        getFolder(rootFolder, true);
                    }
                    catch
                    {
                        try
                        {
                            if (main.cancelled)
                            {
                                r.f("Cancelled Root Mail Folder Retrival.");
                                return;
                            }
                            getFolder(rootFolder, true);
                        }
                        catch
                        {
                            fileLog.outlog("Unable to open root folder for " + pstPathReference, true);
                        }
                    }               
                    break;
                case getMode.getFolder: //unused
                    //r.f("Openning: " + pstPath);
                    getFolder(rootFolder, true);
                    break;
                case getMode.openMail:
                    try
                    { 
                        r.f("Openning mail from " + pstPath);
                        openEmail2(outlookNs.GetItemFromID(entryID.Trim()));
                    }
                    catch
                    {
                        r.f("Pst close has failed, will try again when this application exits.");
                    }
                    break;
                case getMode.debug:
                    break;
                case getMode.verify:
                    //will reach here to close store if verify is sucessful, otherwise storeIsOpenAble will be false
                    closeStore();
                    break;
            }
        }
        public void closeStore()
        {
            var closer = new OutlookManagerI();
            closer.CloseStoreByPath(pstPathReference);
            
        }
        void openEmail2(object openMe)
        {
            if (openMe is MailItem)
            {
                r.f("openning email to display");
                MailItem mi = openMe as MailItem;
                mi.Display();
                
            }
            else
            {
                r.f("mail item not found to be displayed");

            }
        }

        public List<mailData> getMailData()
        {
            return collection;
        }
        void getFolder(MAPIFolder folder, bool isRoot = false)
        {
            //r.f("Retreiving item in folder: " + folder.Name);
            if (isRoot)
            {
                getMailItems(folder);
            }
            foreach (MAPIFolder f in folder.Folders) //will try this twice
            {
                try
                {
                    getMailItems(f); //wait for this to complete then..
                    getFolder(f); //find more folders to process items
                    if (main.cancelled)
                    {
                        r.f("Cancelled Mail Folder Retrival.");
                        return;
                    }
                }
                catch
                {
                    try
                    {
                        getMailItems(f); //wait for this to complete then..
                        getFolder(f); //find more folders to process items
                        if (main.cancelled)
                        {
                            r.f("Cancelled Mail Folder Retrival.");
                            return;
                        }
                    }
                    catch
                    {
                        fileLog.outlog("Unable to open inner mail folder for " + pstPathReference, true);
                    }    
                }
            }
        }


        void getMailItems(MAPIFolder folder)
        {
            //r.f("Getting item for folder: " + folder.Name);  
            //r.f("Item count is: " + folder.Items.Count);
            //Console.ReadLine();

            //collection of workers, limitWorkers is concurrent workers, if limit is reach, then wait for them to complete before adding more workers
            var senderWorkers = new List<Task>();
            var currentWorker = 0;
            var limitWorkers = 1000;

            foreach (object i in folder.Items)
            {                
                if (i is MailItem)
                {
                    MailItem mi = i as MailItem;
                    
                    var singleInsert = new Action<MailItem, string>(delegate (MailItem mail, string folderNamePass)
                    {
                        //fileLog.outlog("IMPORTING subject => " + mail.Subject, false); //if enable, lots of logs in console
                        var db = new dbConnection(main.mySettings);
                        db.InsertMailDataSingle(dbMail.buildQuery(MailItemToMailData(mail, folderNamePass)));
                        db.disposeConnection();
                        Marshal.ReleaseComObject(mail);
                    });

                    if (!isDirect)
                    {
                        collection.Add(MailItemToMailData(mi, folder.Name));
                    }else //direct mode, query will be sent just for this item
                    {
                        main.statz.statz.pushed++;
                        if (main.mySettings.multithread)
                        {
                            ////pool
                            main.currentPooling++;
                            if (main.currentPooling > main.limitPoolingCalls)
                            {
                                dbConnection.ResetPool();
                                //fileLog.outlog("Limit pooling calls reached, pool will reset", false);
                            }
                            senderWorkers.Add(Task.Run(() =>
                            {
                                try
                                {
                                    singleInsert(mi, folder.Name);
                                }
                                catch
                                {
                                    fileLog.outlog("Fail to import item Outlook may have crashed.", true);
                                }
                            }));
                            currentWorker++;

                            //debug
                            //r.f("active workers: " + currentWorker + " total: " + senderWorkers.Count);
                        }
                        else
                        {
                            ////non-pool
                            var con = new dbConnection(main.mySettings);
                            con.InsertMailDataSingle(dbMail.buildQuery(MailItemToMailData(mi, folder.Name)));
                            con.disposeConnection();
                        }

                        if (main.statz.isThrottled() && !main.cancelled)
                        {
                            ////full stop if disconnected///
                            var reportConnection = false;
                            while (main.statz.isThrottleConnection()) //complete stop
                            {
                                if (!reportConnection)
                                {
                                    reportConnection = true;
                                    fileLog.outlog("Unable to connect to database.", true);
                                }
                                r.f("No connection to db");
                                Thread.Sleep(3000);
                            }
                            ////full stop if disconnected///
                            Thread.Sleep(main.statz.statz.throttleDelay);
                        }
                    }  
                    //r.f(mi.Subject);
                }
                if (main.cancelled)
                {
                    r.f("Cancelled Mail item Retrival.");
                    return;
                }

                //when true, limitedworkers has reached so wait for them to complete before queue up more workers in parallel
                if (currentWorker > limitWorkers)
                {
                    //fileLog.outlog("Sender worker limit is reached, awaiting full completion", false);
                    Task.WaitAll(senderWorkers.ToArray());
                    currentWorker = 0;
                }

            }
            Task.WaitAll(senderWorkers.ToArray()); //don't exit until all workers are finished
        }

        mailData MailItemToMailData(MailItem mailItem, string folderName)
        {
            mailData md = new mailData();
            md.fileName = Regex.Replace(pstLocation, @"\\", @"\\"); //first one is regex escape, second is actual replacement                                                                        //data.fileName = pstLocation.Replace(@"\\", @"\\");
            md.folderName = folderName;

            
            try
            {
                md.date = mailItem.ReceivedTime;
                md.entryID = mailItem.EntryID.ToString().Trim();
                md.body = mailItem.Body;
                md.subject = mailItem.Subject;
                md.recipient = r.concatinateRecipients(mailItem.Recipients);

            }
            catch
            {
                fileLog.outlog("Error importing from " + pstLocation + ". Inside folder " + folderName, true);
                md.date = DateTime.Now;
                md.entryID = "ENTRY ID ERROR";
                md.body = "BODY IMPORT ERROR";
                md.subject = "SUBJECT IMPORT ERROR";
                md.recipient = "RECIPIENT ERROR";
            }
            
            
            //r.f(md.recipient);
            return md;
        }
    }
}
