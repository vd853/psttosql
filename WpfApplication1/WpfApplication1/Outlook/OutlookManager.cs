﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.Office.Interop.Outlook;

namespace WpfApplication1
{
    static class OutlookManager
    {
        public class PSTUsage{
            public string fileName;
            public int open; //if open is > 0, it is being used
            public PSTUsage(string _fileName, int _open)
            {
                fileName = _fileName;
                open = _open;
            }
        }
        public static List<string> openPst;
        public static string currentProfile;

        public static void addStore(string fileName)
        {
            openPst.Add(fileName);
        }

        public static void closeAllStores() //only use when closing the entire application
        {
            var closer = new OutlookManagerI();
            closer.CloseEveryStore();
        }
        public static void displayStoreStatus()
        {
            foreach (var item in openPst)
            {
                r.f("Has open " + item);
            }
        }

        public static void RestartOutlook(bool isLong = false)
        {
            //use to kill out look if app.quit doesn't quit within 20 seconds
            var timeOutCancelTokenSource = new CancellationTokenSource();
            var timeOutCancelToken = timeOutCancelTokenSource.Token;

            var offTime = 40000;
            var app = new Application();
            var outlookNs = app.GetNamespace("MAPI");
            currentProfile = outlookNs.CurrentProfileName;

            //another thread to force quit Outlook
            Task.Run(() =>
            {
                Thread.Sleep(offTime / 4); //default offTime / 4 timeout in a quater of resting time
                if (!timeOutCancelToken.IsCancellationRequested)
                {
                    OutlookKiller();
                }
            }, timeOutCancelToken);

            try
            {
                app.Quit(); //this will not move if Outlook has any exit prompt
                timeOutCancelTokenSource.Cancel();
            }catch
            {
                fileLog.outlog("Outlook restart was forced. Exit maybe blocked by Outlook an prompt.", true);
            }
            
            while (checkForOutlook())
            {
                Thread.Sleep(1000);
            }
            if (isLong)
            {
                Thread.Sleep(offTime*2);
            }
            else
            {
                Thread.Sleep(offTime); //long outlook off
            }

            Microsoft.Win32.RegistryKey key =
                Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"Software\\microsoft\\windows\\currentversion\\app paths\\OUTLOOK.EXE");
            string path = (string)key.GetValue("Path");
            if (path != null) System.Diagnostics.Process.Start("OUTLOOK.EXE", "/profile "+currentProfile);

            while (!checkForOutlook())
            {
                Thread.Sleep(1000);
            }
            Thread.Sleep(2000); //prevent the select profile dialog
            try
            {
                app = new Application();
                outlookNs = app.GetNamespace("MAPI");
            }catch
            {
                var keepTrying = true;
                while (keepTrying)
                {
                    try
                    {
                        app = new Application();
                        outlookNs = app.GetNamespace("MAPI");
                        keepTrying = false;
                    }
                    catch
                    {
                        keepTrying = true;
                    }
                    Thread.Sleep(1000);
                }
            }
            Thread.Sleep(10000); //long outlook wake
            //r.v("outlook ready");
        }

        public static bool checkForOutlook()
        {

            if (Process.GetProcessesByName("OUTLOOK").Length > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void OpenOutlook(string profile = "")
        {
            Microsoft.Win32.RegistryKey key =
                Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"Software\\microsoft\\windows\\currentversion\\app paths\\OUTLOOK.EXE");
            string path = (string)key.GetValue("Path");
            if (profile == "")
            {
                if (path != null) System.Diagnostics.Process.Start("OUTLOOK.EXE");
                return;
            }
            if (path != null) System.Diagnostics.Process.Start("OUTLOOK.EXE", "/profile " + profile);
        }
        public static void OutlookKiller()
        {
            foreach (Process proc in Process.GetProcessesByName("OUTLOOK"))
            {
                proc.Kill();
            }  
        }
    }
    class OutlookManagerI
    {
        private string fileName;
        public OutlookManagerI()
        { 
        }

        public void CloseAnyStore()
        {
            var app = new Application();
            var outlookNs = app.GetNamespace("MAPI");
            foreach (var outlookNsStore in outlookNs.Stores)
            {
                Store s = null;
                try
                {
                    s = outlookNsStore as Store;
                    if (s.FilePath != null)
                    {
                        if (s.FilePath != outlookNs.DefaultStore.FilePath)
                        {
                            if (s.FilePath.Length > 0)
                            {
                                outlookNs.RemoveStore(s.GetRootFolder());
                            }
                        }
                    }
                }
                catch
                {
                    r.v("cannot remove " + s.DisplayName);
                }
                
            }
        }
        public void CloseStoreByPath(string PSTPath)
        {
            var willClose = false;
            MAPIFolder folderName = null;
            var app = new Application();
            var outlookNs = app.GetNamespace("MAPI");
            foreach (var outlookNsStore in outlookNs.Stores)
            {
                var s = outlookNsStore as Store;
                if (s.FilePath == PSTPath)
                {
                    folderName = s.GetRootFolder();
                    willClose = true;
                }
            }
            if (willClose)
            {
                outlookNs.RemoveStore(folderName);
            }
        }
        public string getStoreName(string _fileName)
        {
            fileName = _fileName;
            string storeName = "null";
            Application app = new Application();
            NameSpace outlookNs = app.GetNamespace("MAPI");
            try
            {
                foreach (Store item in outlookNs.Stores) //HOW TO GET STORE NAME!
                {
                    if (item.FilePath == null)
                    {
                        continue; //some stores don't have path
                    }

                    var itemPath = item.FilePath as string; //if store is already open, this return as an object
                    itemPath = new string(itemPath.ToLower().ToCharArray());
                    //r.f("Total store found: " + outlookNs.Stores.Count);
                    //r.f("Search store name: " + item.DisplayName + " for file: " + fileName);
                    //r.f("Compare with: " + itemPath);
                    var comparison = itemPath.Equals(fileName, StringComparison.CurrentCultureIgnoreCase);
                    if (comparison)
                    {
                        storeName = item.DisplayName;
                        r.f("Store name found: " + storeName);
                        break;
                    }
                }
            }
            catch
            {
                var msg = new string("Close PST that may have been removed from path. And try import again.".ToCharArray());
                fileLog.outlog(msg, true);
            }
            return storeName;
        }
        public void CloseEveryStore()
        {
            foreach (var s in OutlookManager.openPst)
            {
                CloseStoreByPath(s);
            }
            //Application app = new Application();
            //NameSpace outlookNs = app.GetNamespace("MAPI");
            //foreach (string item in OutlookManager.openPst)
            //{
            //    try
            //    {
            //        //r.v("Removing: " + item);
            //        MAPIFolder rootFolder = outlookNs.Stores[getStoreName(item)].GetRootFolder();
            //        outlookNs.RemoveStore(rootFolder);
            //    }
            //    catch
            //    {
            //        //r.v(item + " may have been removed");
            //        //fileLog.outlog(item + " may have been removed", true);
            //    }
            //}
        }
    }
}
