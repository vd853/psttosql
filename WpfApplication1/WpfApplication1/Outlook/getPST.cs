﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;
using Utilities;

namespace WpfApplication1
{
    public class getPST
    {
        public class PSTFileProfiler
        {
            public List<PSTProfileModel> fileProfile { get;}
            public string searchPath { get; set; }

            public PSTFileProfiler(string searchPath, string[] pstPaths)
            {
                this.fileProfile = fileProfile;
                this.searchPath = searchPath;

                int i = 0;
                fileProfile = new List<PSTProfileModel>();
                foreach (var paths in pstPaths)
                {
                    fileProfile.Add(new PSTProfileModel(paths, getProfileName(paths), i));
                    i++;
                }
            }

            private string getProfileName(string path)//use to make table names in database. naming convention is very specific
            {
                var profileNameLength = 20;
                var search = searchPath;
                if (search[search.Length - 1] == @"\".ToCharArray()[0]) //removes extra slash at the end in present in searchpath
                {
                    search = search.Substring(0, search.Length - 1);
                }
                path = path.Replace(search, ""); //removes the searchPath part of the whole path
                var reg = new Regex(@"(?<=\\)[\w\s]+");
                var include = reg.Matches(path).Cast<Match>().Select(match => match.Value).ToList();
                for (var i = 0; i < include.Count; i++) //remove space from include
                {
                    include[i] = include[i].Replace(" ", "");
                }
                var exclude = reg.Matches(searchPath).Cast<Match>().Select(match => match.Value).ToList();
                //var removed = include.Except(exclude).ToList();
                var first = include[0];
                var root = exclude[exclude.Count-1];

                if (StringValidator.StringJustNumber(first)) //if the string only contains numbers, convert numbers to words
                {
                    first = StringFormatter.NumberToWords(StringFormatter.stringToInt(first));
                }
                var clean = StringFormatter.WordsOnly(first, profileNameLength);

                if (StringValidator.StringJustNumber(root)) //if the string only contains numbers, convert numbers to words
                {
                    root = StringFormatter.NumberToWords(StringFormatter.stringToInt(root));
                }
                var cleanRoot = StringFormatter.WordsOnly(root, profileNameLength);

                if (clean.Length == 0 || include.Count == 1) //if all else false 
                {
                    clean = "unknownprofile";
                }
                if (include.Count == 1) //if include.count == 1 the pst is not in a subfolder, profile will be name by most upper folder layer
                {
                    clean = cleanRoot;
                }
                clean = clean.ToLower();
                return clean;
            }
        }

        public class PSTProfileModel
        {
            public string fileLocation { get; set; }
            public string profileName { get; set; }
            public int index { get; set; }
            public bool verified { get; set; }
            public bool execute { get; set; }

            public PSTProfileModel(string fileLocation, string profileName, int index)
            {
                this.execute = true;
                this.verified = false;
                this.fileLocation = fileLocation;
                this.profileName = profileName;
                this.index = index; 
            }
        }

        private List<string> PSTs;
        public List<PSTProfileModel> PSTProfile { set; get;}
        public getPST(string searchPath)
        {
            PSTs = new List<string>();
            getFolder(searchPath, true);
            var p = new PSTFileProfiler(searchPath, r.ListToArryString(PSTs));
            PSTProfile = new List<PSTProfileModel>(p.fileProfile); 
        }
        public string[] getAllPST()
        {
            return r.ListToArryString(PSTs);
        }

        //removes from PSTProfile if entry does not exist in RemoveSearchPaths
        public List<PSTProfileModel> RemoveFrom(string[] RemoveSearchPaths)
        {
            foreach (var pstProfileModel in PSTProfile)
            {
                var dontRemove = false; //remove by default
                foreach (var removeSearchPath in RemoveSearchPaths)
                {
                    if (removeSearchPath == pstProfileModel.fileLocation) //if found, don't remove
                    {
                        dontRemove = true;
                        break;
                    }
                }
                if (!dontRemove)
                {
                    pstProfileModel.execute = false;
                }
            }
            return PSTProfile;
        }

        public List<PSTProfileModel> GetExecuteList()
        {
            var returnList = new List<PSTProfileModel>();
            foreach (var pstProfileModel in PSTProfile)
            {
                if (pstProfileModel.execute)
                {
                    returnList.Add(pstProfileModel);
                }
            }
            return returnList;
        }
        private void getFolder(string rootPath, bool isRoot = false)
        {
            if (isRoot)
            {
                getFiles(rootPath); //get files just in root path
            }
            string[] folder = Directory.GetDirectories(rootPath);
            foreach (var f in folder)
            {
                //Console.WriteLine(f);
                getFiles(f);
                getFolder(f);
            }
        }
        void getFiles(string rootPath)
        {
            string[] files = Directory.GetFiles(rootPath);
            foreach (var f in files)
            {
                if (Path.GetExtension(f) == ".pst")
                {
                    PSTs.Add(f);
                }
            }
        }
    }
}
