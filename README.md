# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This application is use to collect a large amount of fixed Outlook PST files and import their searchable content to a SQL server. PST files are often use to archive old emails. PST are stored as a file, and many PST files can be created overtime among different users. Searching for a single email from a ton of unloaded PST files is tedious and time consuming. One would have to open a single PST file at a time, and wait for it to be index before any searching can be done. This application attempts to automate that process by being a mediator between Outlook and a SQL server.

Features:

-Continuous background result buffering for large searches

-Resumes from disconnect during importing

-Recursively discovers all PST files and automatically assigns proper profile names

-Cross searching between multiple PST files

-Enlarge cell preview on mouse hover

-Comprehensive selection manager

-Automatically index and optimize the SQL server

-Logs all activities

![Scheme](/1.png)
![Scheme](/2.png)
### How do I get set up? ###

Compiled version is in my Google drive as "PST Archiver Searcher". You will need a MySQL server up and running, everything else can be done through this application. I have tested it with Outlook 2010 and 2016.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact